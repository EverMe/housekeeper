App({
  data: {
    shopName: '请选择门店',
    info:null,
    scene:'',//扫码
    showModalStatus: false,//弹窗
    showModalStatus1: false,//扫码输入手机号弹窗
    resultExpressId:'',//扫码单号
    shopId:'',//店铺id
    shopAddress:'',//店铺地址
    shopPhone: '',//店铺电话
    price: '',//店铺单价
    token:'',
    dataList:null,//投递列表
    number:'',//投递件数量
    total:'',//投递总价
    overdueData: [],//已逾期
    haveTakeData: [],//已自提
    waitTakeData: [],//待自提
    allData:[],//全部
  },
  onLaunch: function () {
    // var scenc = wx.getStorageSync('scene');
    // if (wx.getStorageSync('scene') !== ''){
    //   this.data.scene = wx.getStorageSync('scene') || '';//扫码进店
    // }
    this.data.info = wx.getStorageSync('info') || null;//token,手机号
    this.data.token = wx.getStorageSync('token') || null;//token,手机号
    this.data.resultExpressId = wx.getStorageSync('resultExpressId') || '';//扫码单号
    
    this.data.shopName = wx.getStorageSync('shopName') || '请选择门店';//请选择门店
    this.data.shopId = wx.getStorageSync('shopId') || '';//店铺id
    this.data.shopAddress = wx.getStorageSync('shopAddress') || '';//店铺地址
    this.data.shopPhone = wx.getStorageSync('shopPhone') || '';//店铺电话
    this.data.price = wx.getStorageSync('price') || '';//店铺单价
    this.data.batchId = wx.getStorageSync('batchId') || '';//店铺单价
    this.data.number = wx.getStorageSync('number') || null;//投递件数量
    this.data.total = wx.getStorageSync('total') || null;//投递总价
    this.data.overdueData = wx.getStorageSync('overdueData') || null;//已逾期
    this.data.haveTakeData = wx.getStorageSync('haveTakeData') || null;//已自提
    this.data.waitTakeData = wx.getStorageSync('waitTakeData') || null;//待自提
    this.data.allData = wx.getStorageSync('allData') || null;//待自提
    
    var app = this;
  
    // 版本更新提示
    if (wx.canIUse('getUpdateManager')) {
      const updateManager = wx.getUpdateManager()
      updateManager.onCheckForUpdate(function (res) {
        // 请求完新版本信息的回调
        if (res.hasUpdate) {
          updateManager.onUpdateReady(function () {
            wx.showModal({
              title: '更新提示',
              content: '新版本已经准备好了，是否重启应用更新？',
              success: function (res) {
                if (res.confirm) {
                  // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
                  updateManager.applyUpdate()
                }
              }
            })
          })
          updateManager.onUpdateFailed(function () {
            // 新的版本下载失败
            wx.showModal({
              title: '已经有新版本了哟~',
              content: '新版本已经上线啦~，请您删除当前小程序，重新搜索打开哟~',
            })
          })
        }
      })
    } else {
      // 如果希望用户在最新版本的客户端上体验您的小程序，可以这样子提示
      wx.showModal({
        title: '提示',
        content: '当前微信版本过低，无法使用该功能，请升级到最新微信版本后重试。'
      })
    }
    
  },
  // 登录
  login: function () {
    wx.showLoading({
      title: '请稍后',
      mask:true
    })
    var app = this;
    wx.login({
      success: res => {
        if (res.code) {
          var that = this;
          var jsCode = res.code;
          var params = { jsCode: res.code };
          app.post("login", params, function (data) {   
            that.data.token = data.info.token
            wx.setStorageSync('token', data.info.token)
            wx.hideLoading()
            that.data.info = data.info;
            wx.setStorageSync('info', data.info)  
            if (getCurrentPages().length != 0) {
              getCurrentPages()[getCurrentPages().length - 1].onLoad()
            }
              
            if (app.data.info.Phone == null || app.data.info.Phone == "0" || app.data.info.Phone == "") {
              wx.hideLoading()
              wx.redirectTo({
                url: '../Reg/Reg'
              })
            }   
          });
        } else {
          wx.hideLoading()
          wx.showModal({
            content: '获取用户登录态失败！' + res.errMsg
          })
        }
      }
    });
  },
  // 检查登录状态
  checkLogin:function(){
    var app = this;
    wx.checkSession({
      success: function (e) { 
        console.log('登录态未过期')  //登录态未过期
        let phone = wx.getStorageSync('info').Phone;
        if (!phone || phone == '0') {
          app.login();
        }
        
      },
      fail: function () { 
        console.log('登录态过期了')  //登录态过期了
       app.login();
      
      }
    });
  }, 

  // 重新封装POST,方便调用;
  post: function (api, params, callBack, base = "base") {
    wx.request({
      url: this.getUrl(api, base),
      data: params,
      method: "POST",
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      success: res => {
        // console.log(res)
        var code = res.data.code, msg = res.data.msg, data = res.data.data;
        if (code == "10000") {
          callBack && callBack(data);
        } else {
          console.log(res)
          wx.showModal({
            title: '',
            content: msg ? msg + "":'系统异常，请稍后重试',
            showCancel: false, //不显示取消按钮
            confirmText: '确定'
          }) 
          wx.hideLoading();
        }
      },
      fail: function (err) {
        wx.showModal({
          title: '网络异常',
          content: '当前网络环境较差，请在网络恢复正常后重试',
          showCancel: false,
          confirmText: "我知道了"
        })
      }
    })
  },

  //封装拼凑服务器接口地址
  getUrl: function (api = "", baseServer = "base") {
    return this.globalData.baseUrl[baseServer] + '/' + this.globalData.apiUrl[api];
  },


  //本地存储全局设置参数
  globalData: {
    baseUrl: {
      base: "https://mapi.shequbow.com",//基础服务地址
      // base: "http://119.23.238.132:9660",//基础服务地址
      mapServer: "https://mapServer",//示例:地图服务地址
      msgServer: "https://msgServer",//示例:消息通道服务地址
      pushServer: "https://pushServer",//示例:推送通道服务地址
    },
    apiUrl: {
      login: "login",//api接口:登录
      info: "get/account/info",//api接口:个人资料
      userinfo: "update/user/info",//api接口:更新用户信息
      userlist: "get/user/amount/list",//api接口:用户充值消费记录
      company: "update/user/company/id",//api接口:公司名称
      station: "update/user/station/name",//api接口:分部名称
      userid: "update/user/id/number",//api接口:身份证
      wxinfo: "upload/account/wx/info",//api接口:完善用户信息
      searchShop: "search/shop",//api接口:店铺搜索
      shoplist: "get/shop/info",//api接口:门店详情
      express: "batch/entry/express",//api接口:录入订单
      batchlist: "get/shop/batch/info/list",//api接口:投递列表
      batchid: "get/express/list/by/batch/id",//api接口:投递订单
      cancelbatch: "cancel/batch",//api接口:撤回订单
      batchnumder: "get/express/count/by/batch/id",//api接口:订单数量
      batchdetail: "get/express/detail/by/express/id",//api接口:订单详情
      balance: "get/account/balance",//api接口:获取余额
      pay: "wx/pay",//api接口:微信支付
      companyList: "common/get/company/list",//api接口:微信支付
      searchlist: "miniprog/searchlist",//api接口:查看搜索订单
      expressphone: "miniprog/expressphone",//api接口:转换手机号
      getphone: "miniprog/getphone", //api接口:获取手机号
      register:'register',//注册
      getprice: 'getprice',//充值金额表
      getparams:'getparams',//计数投递
      recharge:'notational/delivery',//计数支付
      updphone:'updphone',//修改收件人电话
      updexpressnum: 'updexpressnum',//修改运单号
      updframekey: 'updframekey',//修改货架号
      msgnotification:'msgnotification',//获取模板消息
      getoverdue:'getoverdue',//获取逾期件
      cabpwd:'update/cabpwd',//修改柜子密码
      authorizestorelist:'authorizestorelist',//授权店铺列表
      authorizestore:'authorizestore',//授权店铺
      historystore:'historystore',//门店历史
      overdueamount:'overdueamount',//逾期数量
      getreturnrecord:'getreturnrecord',//退件记录
      getsms: 'getsms',//获取验证码
      resendmsg:'resendmsg',//重发短信
      chestNum:'get/cabinet/count/by/batch/id',
      searchChest:'miniprog/searchcabinetlist',//搜索柜子订单
      ChestList:'get/cabinet/list/by/id',//柜子列表
      enterChestRecod:'get/cabinet/info/by/id',//入柜记录
      getAllChest:'get/all/delivered/cabinets',//获取已投快递柜
      NewChestList:'get/cabinet/list/by/id/v2',//快件投柜列表
      emptyChestSearch:'cabinetlist',//空柜查询
      setcabinets:'setcabinets',//授权柜子
      newChestNum:'get/cabinet/count/by/batch/id/v2',//新柜子数量
      abnExpress:'get/exception/list',//异常件
      msgcallback:'msgcallback',//短信发送详情
    },
    userInfo: null,
    tabbar: {
      "color": "#333",
      "selectedColor": "#f65d0b",
      "backgroundColor": "#ffffff",
      "position": "bottom",
      "list": [
        {
          "pagePath": "/pages/index/index",
          "text": "首页",
          "iconPath": "/image/nav_icon_home_default_@2x.png",
          "selectedIconPath": "/image/nav_icon_home_selected_@2x.png",
          "selected": true
        },
        {
          "pagePath": "/pages/user/user",
          "text": "我的",
          "iconPath": "/image/nav_icon_me_default_@2x.png",
          "selectedIconPath": "/image/nav_icon_me_selected_@2x.png",
          "selected": false
        }
      ]
    }  
  }

})
