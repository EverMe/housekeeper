var app = getApp()
Page({
  data: {
    nickname:"",//姓名
    idNumber :'',//身份证
    phone:'',//手机号
    companyName: '',//公司名称
    regionArray: ['深圳市', '福州市', '厦门市','玉溪市'],//城市列表
    region: '0',//默认显示城市
    getCodeText:'获取验证码',
    getCodeStatus:true,
    code:''
  },
  onLoad:function(e){
    
  },
  onShow:function(){
    if (app.data.info) {
      this.setData({
        companyName: app.data.info.companyName
      })
    }
    
  },
  formSubmit: function (e) {
    var that =  this;
    var regphone = /^1[0-9]{10}$/;//手机号
    var regIdNo = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;//身份证
    var han = /^[\u4e00-\u9fa5]+$/;//姓名
    if (that.data.nickname == "") {
        wx.showToast({
          image: '../../image/icon_err.png',
          title: '请输入姓名',
        })
    } else if (!han.test(that.data.nickname)) {
      wx.showToast({
        image: '../../image/icon_err.png',
        title: '请输入汉字姓名',
        duration: 2000
      })
    } else if (!regphone.test(that.data.phone)){
      wx.showToast({
        image: '../../image/icon_err.png',
        title: '手机号有误！',
        duration: 2000
      })
    } else if (that.data.phone.length == '') {
      wx.showToast({
        image: '../../image/icon_err.png',
        title: '请输入的手机号',
        duration: 2000
      })
    } else if (that.data.phone.length < 11) {
      wx.showToast({
        image: '../../image/icon_err.png',
        title: '手机号长度有误！',
        duration: 2000
      })
    } else if (that.data.idNumber.length == "") {
      wx.showToast({
        image: '../../image/icon_err.png',
        title: '请输入身份证号',
        duration: 2000
      })
    } else if (!regIdNo.test(that.data.idNumber)){
      wx.showToast({
        title: '请输入正确的身份证号',
        duration: 2000
      })
    } else if (that.data.companyName == "") {
      wx.showToast({
        title: '请选择公司',
        duration: 2000
      })
    } else if (that.data.code == "") {
      wx.showToast({
        title: '请输入验证码',
        duration: 2000
      })
    }else{
      wx.showLoading({
        title: '请稍后',
        mask:true
      })
      var params = {
        token: app.data.info.token,
        nickname: that.data.nickname,
        phone: that.data.phone,
        identi_no: that.data.idNumber,
        com_id: app.data.info.companyId,
        city: that.data.region,
        code:that.data.code
      };
      app.post("register", params, function (data) {
        console.log(data)
        app.data.info= data;
        app.data.info.Phone = that.data.phone;
        app.data.info.nickname = that.data.nickname;
        app.data.token = data.token
        wx.setStorageSync('info', data)
        wx.setStorageSync('token', data.token)
        if (app.data.scene){
          wx.redirectTo({
            url: '../scanStore/scanStore'
          })
        }else{
       
          wx.hideLoading()
          wx.redirectTo({
            url: '/pages/index/index',
            success:function(res){
              wx.showToast({
                title: '注册成功',
              })
            }
          })
       
         
        }
        
      });
    }
  },
  //姓名
  nickNameInput:function(e){
    this.setData({
      nickname: e.detail.value
    })
  },
  phoneInput: function (e) {
    this.setData({
      phone: e.detail.value
    })
  },
  //身份
  IdInput: function (e) {
    this.setData({
      idNumber: e.detail.value
    })
  },
  codeInput:function(e){
    this.setData({
      code: e.detail.value
    })
  },
  //城市
  bindRegionChange: function (e) {
    this.setData({
      region: e.detail.value
    })
  },
  //公司名称
  toCompany: function () {
    wx.setStorageSync('companyStauts',1)
    wx.navigateTo({
      url: '../company/company'
    })
  },
  getCode:function(){
    var that = this;
    if(that.data.getCodeStatus){
      var regphone = /^1[0-9]{10}$/;//手机号
       if (that.data.phone.length == '') {
        wx.showToast({
          image: '../../image/icon_err.png',
          title: '请输入手机号',
          duration: 2000
        })
        return;
      }
      else if (!regphone.test(that.data.phone)) {
        wx.showToast({
          image: '../../image/icon_err.png',
          title: '手机号有误！',
          duration: 2000
        })
        return;
      }
      
      var params = {};
      params.token = app.data.token;
      params.phone = that.data.phone;
      app.post('getsms',params,function(data){
        console.log(data)
        wx.showToast({
          title: '已发送',
          duration: 2000
        })
        time(timeNum);
      })

      var timeNum = 120;
      function time(timeNum) {
        that.setData({
          getCodeText: timeNum + '秒后重试',
          getCodeStatus: false
        })
        if (timeNum == 0) {
          that.setData({
            getCodeText: '获取验证码',
            getCodeStatus: true
          })
        }
        else {
          setTimeout(function () {
            timeNum = timeNum - 1;
            time(timeNum);
          }, 1000)

        }
      }

      
    }
    else{
      return;
    }
    
  }
})