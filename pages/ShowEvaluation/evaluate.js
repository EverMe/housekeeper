// pages/ShowEvaluation/evaluate.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tabList: ['已逾期', '待自提', '已完成'],
    current: 0,
    switchTab: [],//详细订单的展开状态(滞留件)
    switchTab2: [],//详细订单的展开状态（待取件）
    switchTab3: [],//详细订单的展开状态（已完成）
    showpage: true,
    canSelectTime: ['最近1周', '最近2周', '最近3周', '最近1月'],
    timeWindowstatus: false,//是否显示选择时间窗口
    selectedTime: '全部',//默认 最近1周
    keyvalue: [2, 3],
    sureSetPhone: false,//是否可以操作确认修改电话号码
    getPhone: 15797769097,
    firstReset: true,//初始状态出现清空按钮
    PhoneModel: false,//是否显示修改手机model
    phoneFocus: true,//输入框光标
    chestRecord: false,//入柜记录显示状态
    numList: [],//各类订单数量
    openSuccess: false,//开柜成功模态窗默认状态
    SIGNEDcanScroll: true,//地址所在scroll-view默认可以上下滑动(已完成)
    OVERDUEcanScroll: true,//地址所在scroll-view默认可以上下滑动(已逾期)
    WAITTAKEcanScroll: true,//地址所在scroll-view默认可以上下滑动（待自提）
    OVERDUE2: [],//用于存放已逾期件的初始化数组
    WAITTAKE2: [],//用于存放待自提件的初始化数组
    SIGNED2: [],//用于存放已完成件的初始化数组
    OVERDUEpage: [],//已逾期件的初始化页数
    WAITTAKEpage: [],//待自提件的初始化页数
    SIGNEDpage: [],//已完成件的初始化页数
    reSendStatus: false,//重发短信弹窗的状态
  },

  // 监听轮播页
  swiperChange: function (e) {
    this.setData({
      current: e.detail.current
    })
  },

  // 点击tab
  selectTab: function (e) {
    var index = e.currentTarget.dataset.index;
    this.setData({
      current: index
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    wx.showLoading({
      title: '加载中',
      mask: true
    })
    var token = wx.getStorageSync('token');
    console.log(this.data.WAITTAKE2)

    // 在详情页修改电话，订单页同步
    if (wx.getStorageSync('backData')) {
      let backData = wx.getStorageSync('backData');
      let bgindex = wx.getStorageSync('backData').back_bgindex;
      let smindex = wx.getStorageSync('backData').back_smindex;
      if (backData.back_current == 0) {
        this.data.OVERDUE2[bgindex][smindex].phone = wx.getStorageSync('backPhone')
        this.setData({
          OVERDUE2: this.data.OVERDUE2
        })
        wx.setStorageSync('backData', false)
      }
      else if (backData.back_current == 1) {
        this.data.WAITTAKE2[bgindex][smindex].phone = wx.getStorageSync('backPhone')
        this.setData({
          WAITTAKE2: this.data.WAITTAKE2
        })
        wx.setStorageSync('backData', false)
      }
      else if (backData.back_current == 2) {
        this.data.SIGNED2[bgindex][smindex].phone = wx.getStorageSync('backPhone')
        this.setData({
          SIGNED2: this.data.SIGNED2
        })
        wx.setStorageSync('backData', false)
      }

      // 撤回同步
      if (wx.getStorageSync('chehui')) {
        if (backData.back_current == 0) {
          this.data.OVERDUE2[bgindex][smindex].splice(1, 1)
          wx.setStorageSync('chehui', false)
        }
        else if (backData.back_current == 1) {
          this.data.WAITTAKE2[bgindex][smindex].splice(1, 1)
          wx.setStorageSync('chehui', false)
        }
      }
    }

    //  请求各类型订单的数量
    app.post('newChestNum', { token: wx.getStorageSync('token')}, (data) => {
      this.data.numList[0] = data.overdue;
      this.data.numList[1] = data.waitTakeCount;
      this.data.numList[2] = data.haveTakeCount;
      this.setData({
        numList: this.data.numList
      })
    })

    // 请求柜子地址列表
    app.post('getAllChest', { token: wx.getStorageSync('token') }, (data) => {
      wx.hideLoading()
      wx.hideNavigationBarLoading();
      // 停止下拉动作  
      wx.stopPullDownRefresh();
      this.setData({
        allChest: data.list
      })
     
      for (let i = 0; i < this.data.allChest.length; i++) {
        this.data.OVERDUEpage[i] = 1;
        this.data.WAITTAKEpage[i] = 1;
        this.data.SIGNEDpage[i] = 1;
        // 已逾期数据处理
        if (this.data.allChest[i].overdue != 0) {
         
          this.setData({
            hideOVERDUE: true,//存在非零就隐藏无订单提示
          })
        }
        // 待自提数据处理
        if (this.data.allChest[i].waitTakeCount != 0) {
         
          this.setData({
            hideWAITTAKE: true,//存在非零就隐藏无订单提示
          })
        }
        // 已完成数据处理
        if (this.data.allChest[i].haveTakeCount != 0) {
          this.setData({
            hideSIGNED: true,//存在非零就隐藏无订单提示
          })
        }
        this.setData({
          switchTab: this.data.switchTab,
          switchTab2: this.data.switchTab2,
          switchTab3: this.data.switchTab3,
          OVERDUEpage: this.data.OVERDUEpage,
          WAITTAKEpage: this.data.WAITTAKEpage,
          SIGNEDpage: this.data.SIGNEDpage,
        })
      }
    })
  },

  // 是否展开订单详情(已逾期件)
  switchTab: function (e) {
    var bindindex = e.currentTarget.dataset.index;
    var cabid = e.currentTarget.dataset.cabid;
    this.setData({
      bindindex: bindindex,
      cabid: cabid,
      OVERDUEcanScroll: true
    })
    let params = {
      token: wx.getStorageSync('token'),
      cabId: cabid,
      state: 'OVERDUE',
      page: '1',
      size: '10'
    }
    if (!this.data.OVERDUE2[bindindex]) {
      wx.showLoading({
        mask: true
      })
      app.post('NewChestList', params, (data) => {
        wx.hideLoading()
        let OVERDUE2 = `OVERDUE2[${bindindex}]`
        this.setData({
          [OVERDUE2]: data.list,
        })
        this.methods_OVERDUE()

      })
    }
    else {
      this.methods_OVERDUE()
    }

  },

  // 逾期件订单展开交互方法
  methods_OVERDUE: function () {
    let bindindex = this.data.bindindex
    // 排它处理，一次只能打开一个地址
    for (let i = 0; i < this.data.allChest.length; i++) {
      if (i == bindindex) {
        this.data.switchTab[i] = !this.data.switchTab[i]
      }
      else {
        this.data.switchTab[i] = false
      }
    }
    this.setData({
      switchTab: this.data.switchTab,
    })
    this.setData({
      OVERDUEscrollId: 'scrollId' + bindindex
    })

    if (this.data.switchTab[bindindex] == true) {
      this.setData({
        OVERDUEcanScroll: false
      })
    }
    else {
      this.setData({
        OVERDUEcanScroll: true
      })
    }
  },

  // 是否展开订单详情(待取件)
  switchTab2: function (e) {
    var bindindex2 = e.currentTarget.dataset.index;
    var cabid = e.currentTarget.dataset.cabid;
    this.setData({
      bindindex2: bindindex2,
      cabid2: cabid,
      WAITTAKEcanScroll: true
    })
    let params = {
      token: wx.getStorageSync('token'),
      cabId: cabid,
      state: 'WAITTAKE',
      page: '1',
      size: '10'
    }
    if (!this.data.WAITTAKE2[bindindex2]) {
      wx.showLoading({
        mask: true
      })
      app.post('NewChestList', params, (data) => {
        wx.hideLoading()
        let WAITTAKE2 = `WAITTAKE2[${bindindex2}]`
        this.setData({
          [WAITTAKE2]: data.list,
        })
        this.methods_WAITTAKE()

      })
    }
    else {
      this.methods_WAITTAKE()
    }
  },

  // 待自提订单展开交互方法
  methods_WAITTAKE: function () {
    let bindindex2 = this.data.bindindex2
    // 排它处理，一次只能打开一个地址
    for (let i = 0; i < this.data.allChest.length; i++) {
      if (i == bindindex2) {
        this.data.switchTab2[i] = !this.data.switchTab2[i]
      }
      else {
        this.data.switchTab2[i] = false
      }
    }
    this.setData({
      switchTab2: this.data.switchTab2,
    })
    this.setData({
      WAITTAKEscrollId: 'scrollId' + bindindex2
    })

    if (this.data.switchTab2[bindindex2] == true) {
      this.setData({
        WAITTAKEcanScroll: false
      })
    }
    else {
      this.setData({
        WAITTAKEcanScroll: true
      })
    }

  },

  // 是否展开订单详情(已完成)
  switchTab3: function (e) {

    var bindindex3 = e.currentTarget.dataset.index;
    var cabid = e.currentTarget.dataset.cabid;
    this.setData({
      bindindex3: bindindex3,
      cabid3: cabid,
      SIGNEDcanScroll: true
    })
    let params = {
      token: wx.getStorageSync('token'),
      cabId: cabid,
      state: 'HAVETAKEN',
      page: '1',
      size: '10'
    }
    if (!this.data.SIGNED2[bindindex3]) {
      wx.showLoading({
        mask: true
      })
      app.post('NewChestList', params, (data) => {
        wx.hideLoading()
        let SIGNED2 = `SIGNED2[${bindindex3}]`
        for (let i = 0; i < data.list.length; i++) {
          if (data.list[i].stateCN === '已签收') {
            data.list[i].stateCN = '已取件'
          }
        }
        this.setData({
          [SIGNED2]: data.list,
        })
        this.methods_SIGNED()

      })
    }
    else {
      this.methods_SIGNED()
    }
  },

  // 逾期件订单展开交互方法
  methods_SIGNED: function () {
    let bindindex3 = this.data.bindindex3
    // 排它处理，一次只能打开一个地址
    for (let i = 0; i < this.data.allChest.length; i++) {
      if (i == bindindex3) {
        this.data.switchTab3[i] = !this.data.switchTab3[i]
      }
      else {
        this.data.switchTab3[i] = false
      }
    }
    this.setData({
      switchTab3: this.data.switchTab3,
    })
    this.setData({
      SIGNEDscrollId: 'scrollId' + bindindex3
    })

    if (this.data.switchTab3[bindindex3] == true) {
      this.setData({
        SIGNEDcanScroll: false
      })
    }
    else {
      this.setData({
        SIGNEDcanScroll: true
      })
    }

  },

  // 逾期件加载更多

  OVERDUELoadMore: function () {
    wx.showLoading({
      title: '加载中',
      mask: true
    })

    let bindindex = this.data.bindindex;
    let cabid = this.data.cabid;
    this.data.OVERDUEpage[bindindex]++;
    console.log(this.data.OVERDUEpage)
    let params = {
      token: wx.getStorageSync('token'),
      cabId: cabid,
      state: 'OVERDUE',
      page: this.data.OVERDUEpage[bindindex],
      size: '10'
    }
    app.post('NewChestList', params, (data) => {
      wx.hideLoading()
      if (!data.list[0]) {
        wx.showToast({
          title: '没有更多内容了',
          icon: 'none',
          duration: 1500,
        })
        return;
      }
      for (let i = 0; i < data.list.length; i++) {
        this.data.OVERDUE2[bindindex].push(data.list[i])
      }

      this.setData({
        OVERDUE2: this.data.OVERDUE2
      })

    })
    
  },

  // 待自提加载更多

  WAITTAKELoadMore: function () {
    wx.showLoading({
      title: '加载中',
      mask: true
    })
    
    let bindindex2 = this.data.bindindex2;
    let cabid2 = this.data.cabid2
    this.data.WAITTAKEpage[bindindex2]++;
    let params = {
      token: wx.getStorageSync('token'),
      cabId: cabid2,
      state: 'WAITTAKE',
      page: this.data.WAITTAKEpage[bindindex2],
      size: '10'
    }
    app.post('NewChestList', params, (data) => {
      wx.hideLoading()
      if (!data.list[0]) {
        wx.showToast({
          title: '没有更多内容了',
          icon: 'none',
          duration: 1500,
        })
        return;
      }
      for (let i = 0; i < data.list.length; i++) {
        this.data.WAITTAKE2[bindindex2].push(data.list[i])
      }
      this.setData({
        WAITTAKE2: this.data.WAITTAKE2
      })

    })
  },

  // 已完成加载更多

  SIGNEDLoadMore: function () {
    wx.showLoading({
      title: '加载中',
      mask: true
    })
    
    let bindindex3 = this.data.bindindex3;
    let cabid3 = this.data.cabid3;
    this.data.SIGNEDpage[bindindex3]++;
    let params = {
      token: wx.getStorageSync('token'),
      cabId: cabid3,
      state: 'HAVETAKEN',
      page: this.data.SIGNEDpage[bindindex3],
      size: '10'
    }
    app.post('NewChestList', params, (data) => {
      wx.hideLoading()
      if (!data.list[0]) {
        wx.showToast({
          title: '没有更多内容了',
          icon: 'none',
          duration: 1500,
        })
        return;
      }
      for (let i = 0; i < data.list.length; i++) {
        this.data.SIGNED2[bindindex3].push(data.list[i])
      }

      this.setData({
        SIGNED2: this.data.SIGNED2
      })

    })
  },


  // 去详情页
  toDetail: function (e) {
    let detail = e.currentTarget.dataset.item
    detail['epid'] = detail.expressNum;
    detail['datePuton'] = detail.putOnDate;
    detail['datePulloff'] = detail.pullOffDate;

    wx.navigateTo({
      url: '/pages/waybillDetail/detail?detail=' + JSON.stringify(detail),
    })
  },

  // 修改手机的监听事件
  inputBind: function (e) {
    let phone = e.detail.value;
    if (phone.length == 11) {
      this.setData({
        phoneNumber: phone,
        sureSetPhone: true,
        firstReset: false
      })
    }
    else {
      this.setData({
        sureSetPhone: false,
        firstReset: false
      })
    }
  },

  // 删除输入的手机号
  resetPhone: function () {
    this.setData({
      sureSetPhone: false,
      needSetphone: '',
      firstReset: false,
      phoneFocus: true,
    })
  },

  // 暂不修改
  closePhoneModel: function () {
    this.setData({
      PhoneModel: false
    })
  },

  // 弹出修改手机model
  updatePhone: function (e) {
    var phone = e.currentTarget.dataset.phone;
    var expressid = e.currentTarget.dataset.expressid;
    var bigindex = e.currentTarget.dataset.bigindex;
    var index = e.currentTarget.dataset.smindex;

    this.setData({
      PhoneModel: true,
      needSetphone: phone,
      getexpressId: expressid,
      bigindex: bigindex,
      smindex: index,
    })

  },

  // 重发短信取件码
  rsendNote: function (e) {
    var expressid = e.currentTarget.dataset.expressid
    this.setData({
      noteExpressid: expressid,
      reSendStatus: true,
    })
  },

  // 确定发送短信
  sureSend: function () {
    this.setData({
      reSendStatus: false
    })
    app.post('resendmsg', { token: wx.getStorageSync('token'), expressId: this.data.noteExpressid }, (data) => {

      wx.showToast({
        title: '发送成功',
      })
    })
  },

  // 取消重发短信
  cancleSend: function () {
    this.setData({
      reSendStatus: false
    })
  },

  // 进入运单详情页
  openRecodeModel: function (e) {
    let detail = e.currentTarget.dataset.detail;
    let current = e.currentTarget.dataset.current;
    let bgindex = e.currentTarget.dataset.bgindex;
    let smindex = e.currentTarget.dataset.smindex;
    this.setData({
      back_data: {
        back_current: current,
        back_bgindex: bgindex,
        back_smindex: smindex,
      }

    })
    wx.navigateTo({
      url: '/pages/waybillDetail/detail?detail=' + JSON.stringify(detail) + '&backData=' + JSON.stringify(this.data.back_data),
    })
  },

  // 跳转查询页面
  toSearch: function () {
    wx.navigateTo({
      url: '/pages/searchWaybill/waybill',
    })
  },

  // 拨打电话
  callPhone: function (e) {

    var phone = e.currentTarget.dataset.phone;

    wx.makePhoneCall({
      phoneNumber: phone
    })
  },

  // 确认修改手机号

  sureSet: function () {
    var that = this;
    if (this.data.sureSetPhone) {

      app.post('updphone', { token: wx.getStorageSync('token'), expressId: that.data.getexpressId, phone: that.data.phoneNumber }, (data) => {
        this.setData({
          PhoneModel: false
        })
        if (this.data.current == 0) {
          this.data.OVERDUE2[this.data.bigindex][this.data.smindex].phone = that.data.phoneNumber;
          this.setData({
            OVERDUE2: this.data.OVERDUE2
          })
        }
        else {
          this.data.WAITTAKE2[this.data.bigindex][this.data.smindex].phone = that.data.phoneNumber;
          this.setData({
            WAITTAKE2: this.data.WAITTAKE2
          })
        }
        wx.showToast({
          title: '修改成功',
        })

      })
    }

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    wx.showNavigationBarLoading();
    this.setData({
      switchTab: [],//详细订单的展开状态(滞留件)
      switchTab2: [],//详细订单的展开状态（待取件）
      switchTab3: [],//详细订单的展开状态（已完成）
      SIGNEDcanScroll: true,//地址所在scroll-view默认可以上下滑动(已完成)
      OVERDUEcanScroll: true,//地址所在scroll-view默认可以上下滑动(已逾期)
      WAITTAKEcanScroll: true,//地址所在scroll-view默认可以上下滑动（待自提）
      OVERDUE2: [],//用于存放已逾期件的初始化数组
      WAITTAKE2: [],//用于存放待自提件的初始化数组
      SIGNED2: [],//用于存放已完成件的初始化数组
      OVERDUEpage: [],//已逾期件的初始化页数
      WAITTAKEpage: [],//待自提件的初始化页数
      SIGNEDpage: [],//已完成件的初始化页数
    })
    this.onShow();
    
  },

  // 撤回
  callback: function (e) {
    var batchId = e.currentTarget.dataset.batchid;
    var bigindex = e.currentTarget.dataset.bigindex;
    var smindex = e.currentTarget.dataset.smindex;
    var boxCode = e.currentTarget.dataset.boxcode;
    console.log(e.currentTarget.dataset)
    this.setData({
      openSuccessCode: boxCode,
    })
    wx.showModal({
      title: '开柜撤件提醒',
      content: '您即将打开格口并撤回此快件，请您务必确定位于柜机现场，请确定是否开柜撤件？',
      cancelText: '取消',
      cancelColor: '#A8A8A8',
      confirmText: '确定开柜',
      confirmColor: '#EF5D0F',
      success: (res) => {
        if (res.confirm) {
          wx.showLoading({
            title: '请稍后',
            mask: true
          })
          app.post('cancelbatch', { token: wx.getStorageSync('token'), batchId: batchId }, (data) => {
            wx.hideLoading()
            this.onShow();
            // 请求各类订单数量
            // app.post('newChestNum', { token: wx.getStorageSync('token') }, (data) => {
            //   let numList = []
            //   numList.push(data.overdue)
            //   numList.push(data.waitTakeCount)
            //   numList.push(data.haveTakeCount)
            //   this.setData({
            //     numList: numList
            //   })

            // })
            this.setData({
              openSuccess: true,

            })
            if (this.data.current == 0) {
              this.data.OVERDUE2[bigindex].splice(smindex, 1)
              this.setData({
                OVERDUE2: this.data.OVERDUE2
              })
            }
            else {

              this.data.WAITTAKE2[bigindex].splice(smindex, 1)
              this.setData({
                WAITTAKE2: this.data.WAITTAKE2
              })
            }

          })
        }
      }
    })

  },

  // 关闭模态窗
  closeMengban: function () {
    this.setData({
      openSuccess: false,
    })
  },
})