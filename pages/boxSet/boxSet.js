// pages/boxSet/boxSet.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
  
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  NewpassWdInput1:function(e){
    this.setData({
      NewpassWdInput1: e.detail.value
    })
  },
  NewpassWdInput2: function (e) {
    this.setData({
      NewpassWdInput2: e.detail.value
    })
  },
  subClick:function(){
   
    var new1 = this.data.NewpassWdInput1;
    var new2 = this.data.NewpassWdInput2;
  
    if(!new1||!new2){
     wx.showModal({
        title: '',
        content: '密码不能为空',
        showCancel: false, //不显示取消按钮
        confirmText: '确定'
      })
    }
    else if(new1.length < 6 || new2.length < 6) {
      wx.showModal({
        title: '',
        content: '密码为6-8位数字，请重新输入',
        showCancel: false, //不显示取消按钮
        confirmText: '确定'
      })
    }
    else if(new1 !== new2){
      wx.showModal({
        title: '',
        content: '两次密码不一致',
        showCancel: false, //不显示取消按钮
        confirmText: '确定'
      }) 
    }
    else{
      var params = {
        token:wx.getStorageSync('token'),
        newPwd: new1,
        confirmPwd: new2
      };
      app.post("cabpwd", params, function (res) { 
        wx.showToast({
          title: '修改成功',
          icon: 'success',
          duration: 1000,
          success: function () {
            setTimeout(function () {
              wx.navigateBack({
                delta:1
              })
            }, 1000)
          }
        })
         
      })
    }
  }
})