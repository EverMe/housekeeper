// pages/searchWaybill/waybill.js
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    reset:false,
    codeValue:'',
    focus:true,
    noWaybill:true,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  // 去详情页
  toDetail: function (e) {
    let detail = e.currentTarget.dataset.item
    wx.navigateTo({
      url: '/pages/waybillDetail/detail?detail=' + JSON.stringify(detail),
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // 刷新当前查询的快件
    if (this.data.waybillValue) {
      this.searchWaybill()
    }
    
  },

  // 扫码
  scanCode: function() {
    var that = this;
    wx.scanCode({
      success:(res) => {
        this.setData({
          searchText:true,
          codeValue: res.result,
          reset: true,
          waybillValue: res.result,
        })
        that.searchWaybill()
      }
    })  
  },

  // 监听输入框
  searchValue: function(e) {
    this.setData({
      waybillValue:e.detail.value
    })
    if(!this.data.reset) {
      this.setData({
        reset: true,
        searchText: true,
      })
    }
    else if(e.detail.value == '') {
      this.setData({
        reset: false,
        searchText: false,
      })
    }
  },

  // 重置输入框内容
  reset: function() {
    this.setData({
      codeValue:'',
      searchText: false,
      reset: false,
      focus:true
    })
  },

// 搜索请求
  searchWaybill: function() {
    let params = {
      token:wx.getStorageSync('token'),
      keyWord: this.data.waybillValue
    }
    app.post('searchChest',params,(res) => {
      let data = res.list;
      this.setData({
        waybillList: data,
        noWaybill: true,
      })
      if (!data[0]) {
        this.setData({
          noWaybill:false
        })
      }
    })
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }

  // 6931950703724
})