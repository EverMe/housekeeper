// pages/searchDetail/searchDetail.js
const app = getApp();
// import { HTTP } from '../../http.js'
// var http = new HTTP()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    countDownnum:300
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    var params = wx.getStorageSync('searchDetail');
    app.post('batchdetail', params, function (data) {
      console.log(data)
      var list = data;
      console.log(list)
      if (list.msgStatus == '0') {
        list.msgStatusCN = '发送中'
      }
      else if (list.msgStatus == '1') {
        list.msgStatusCN = '发送成功'
      }
      else if (list.msgStatus == 2) {
        list.msgStatusCN = '发送失败'
      }
      if (list.msgType == 0) {
        list.msgTypeCn = '短信'
      }
      else if (list.msgType == 1) {
        list.msgTypeCn = '微信'
      }

      if (list.operateType == '0') {
        list.operateType = '老板录件'
      }
      else if (list.operateType == '1') {
        list.operateType = '快递员录件'
      }

      that.setData({
        expressNum: data.expressNum,
        shopPhone: data.shopPhone,
        expressPosition: data.expressPosition,
        frameKey: data.frameKey,
        createDate: data.createDate,
        datePuton: data.datePuton,
        dateCollect: data.dateCollect,
        datePulloff: data.datePulloff,
        msgTypeCn: list.msgTypeCn,
        operateType: list.operateType,
        expressId: data.expressId,
        phone: data.phone,
        delieverType: data.delieverType,
        keycode: data.keyCode,
        msgStatusCN: list.msgStatusCN
      })
      if (data.expressState == 0) {
        that.setData({
          expressState: '入库交接'
        })
      } else if (data.expressState == 1) {
        that.setData({
          expressState: '签收'
        })
      } else if (data.expressState == 2) {
        that.setData({
          expressState: '退件'
        })
      } else if (data.expressState == 3) {
        that.setData({
          expressState: '待处理'
        })
      } else if (data.expressState == 4) {
        that.setData({
          expressState: '签收,'
        })
      } else if (data.expressState == 6) {
        that.setData({
          expressState: '快递员撤回'
        })
      } else if (data.expressState == 7) {
        that.setData({
          expressState: '销件'
        })
      } else if (data.expressState == 9) {
        that.setData({
          expressState: '合单确认'
        })
      } else {
        that.setData({
          expressState: '其他'
        })
      }
      //签收类型
      if (data.signType == 0) {
        that.setData({
          signType: '正常'
        })
      } else if (data.signType == 1) {
        that.setData({
          signType: '投店'
        })
      } else if (data.signType == 2) {
        that.setData({
          signType: '投柜'
        })
      } else if (data.signType == 3) {
        that.setData({
          signType: '破损件'
        })
      } else if (data.signType == 4) {
        that.setData({
          signType: '错分件,'
        })
      } else if (data.signType == 5) {
        that.setData({
          signType: '送无人电话不接'
        })
      } else if (data.signType == 6) {
        that.setData({
          signType: '拒收'
        })
      } else if (data.signType == 7) {
        that.setData({
          signType: '预约派送'
        })
      } else if (data.signType == 8) {
        that.setData({
          signType: '改地址'
        })
      } else if (data.signType == 9) {
        that.setData({
          signType: '其他异常'
        })
      } else {
        that.setData({
          signType: '未知异常'
        })
      }
      //门店电话
      if (data.shopPhone == null || data.shopPhone == '') {
        that.setData({
          shopPhone: '--'
        })
      } else {
        that.setData({
          shopPhone: data.shopPhone
        })
      }
      //手机号
      if (data.phone == 0) {
        that.setData({
          phone: '--'
        })
      } else {
        that.setData({
          phone: data.phone
        })
      }

    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },
  // 修改收件人电话
  Modifyphone: function () {
    var that = this;
    that.setData({
      showModalStatus1: true,
    })
  },
  cancel:function(){
    var that = this;
    that.setData({
      showModalStatus1: false,
    })
  },
  recipient:function(e){
    this.setData({
      setPhone: e.detail.value
    })
  },
  submit:function(){
    var that = this;
    var params = {};
    params.token = app.data.token;
    params.expressId = that.data.expressId;
    params.phone = that.data.setPhone;
    app.post('updphone',params,function(data){
      that.setData({
        showModalStatus1: false,
        phone: that.data.setPhone,
      })  
    })
  },
  // 重发短信
  resSend: function () {
    
    console.log('进入了重发短信')
    var data = this.data,
      // expressArr = data.expressArr || { expressId: 0 },
      expressId = data.expressId;
    console.log(expressId)
    app.post("resendmsg", { token: app.data.info.token, expressId: expressId }, function (data) {
      wx.showToast({
        title: '短信发送成功',
      })
    });
    var that = this;
    var countDown = setInterval(function() {
      that.data.countDownnum -= 1;
      that.setData({
        countDownnum: that.data.countDownnum
      })
      if (that.data.countDownnum == 0) {
        clearInterval(countDown)
      }
      // console.log(that.data.countDownnum)
    },1000)
  },
})