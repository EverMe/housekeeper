const app = getApp()
Page({
  data: {
    shopName:'',
    price:'',
    amount:'',
    balance:'',
    showModalStatus:false, 
    selected: true,
    selected1: false,
    ntype:'0',
    totalCost:'',
  },
  onLoad:function(options){
    if(app.data.info){
      var that = this;
      that.setData({
        shopName: app.data.shopName,
        amount: '',
        totalCost: '',
      })
      if (app.data.scene) {
        var scene = app.data.scene; console.log(scene);//scene = 'shopId%3A7';
        var scan_url = decodeURIComponent(scene); console.log(scan_url);
        var shops = scan_url.split(":"); console.log(shops);
        var shopId = shops[1]; console.log(shopId);
        app.data.shopId = shopId;
        wx.setStorageSync('shopId', shopId)
        var params = {
          token: app.data.info.token,
          shopId: shopId
        };
        app.post("shoplist", params, function (data) {
          var item = JSON.stringify(data.shopInfo);
          app.data.shopName = data.shopInfo.shopName;
          wx.setStorageSync('shopName', app.data.shopName)
          that.setData({
            shopName: data.shopInfo.shopName
          })
          that.getTotalCost();
        });
      }
    }
  },
  onShow:function(){
    var that =  this;   
    if (app.data.info || app.data.token) {
      if (app.data.info.Phone == null || app.data.info.Phone == "0" || app.data.info.Phone == "") {
        wx.redirectTo({
          url: '../Reg/Reg'
        })
      }
    } else {
      app.login();
    }
  },
  getTotalCost:function(){
    var that = this;
    var params = {
      token: app.data.info.token,
      shopId: app.data.shopId
    };
    app.post("getparams", params, function (data) {
      var totalCost = ((data.price / 100) * that.data.amount).toFixed(2)
        that.setData({
          price: data.price / 100,
          balance: data.balance / 100,
          totalCost: totalCost,
        } )    
    });
  },
  selected: function (e) {
    this.setData({
      selected1: false,
      selected: true,
      ntype: e.currentTarget.dataset.type
    })
  },
  selected1: function (e) {
    this.setData({
      selected: false,
      selected1: true,
      ntype: e.currentTarget.dataset.type
    })
  },
  goTopup:function (e) {
    wx.navigateTo({
      url: '../topUp/topUp'
    })
  },
  settlement:function(){
    var that = this;
    if(that.data.amount == ""){
      wx.showToast({
        title: '请输入要记录的数量',
        icon: 'success',
        duration: 1000
      })
    }else{     
      var params = {
        token: app.data.info.token,
        totalCost: that.data.totalCost * 100,
        type: that.data.ntype,
        amount: that.data.amount,
        price: that.data.price * 100,
        shopId: app.data.shopId
      };
      app.post("recharge", params, function (data) {
        console.log("111")
        if (data.type == "0") {
          // that.setData({
          //   balance: (data.balance / 100).toPrecision(2),
          // })
          wx.showToast({
            title: '支付成功',
            icon: 'success',
            duration: 1000
          })
          setTimeout(function () {
            wx.navigateTo({
              url: '../deliveryRecord/deliveryRecord'
            })
          }, 1000)
        } else if (data.type == "1") {
          var payModel = data;
          wx.requestPayment({
            'timeStamp': '' + payModel.timeStamp,
            'nonceStr': payModel.nonceStr,
            'package': payModel.package,
            'signType': 'MD5',
            'paySign': payModel.paySign,
            'success': function (res) {
              wx.showToast({
                title: '支付成功',
                icon: 'success',
                duration: 1000
              })
              setTimeout(function () {
                wx.navigateTo({
                  url: '../deliveryRecord/deliveryRecord'
                })
              }, 1000)
            },
            'fail': function (res) {
              wx.showToast({
                title: '支付失败',
                duration: 2000
              })
            }
          })
        } 
      });
    }
    
  },
  getAmount: function (e) {
    this.setData({
      amount: e.detail.value,
    })
    this.getTotalCost();
  },
})