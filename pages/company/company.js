const app = getApp()
Page({

  data: {
    companyId:'',
    logoArray:[],
  },
  onLoad: function (options) {
    var that = this;
    if (app.data.info.token) {
      var params = {
        token: app.data.info.token,
        companyId: that.data.companyId,
      };
      app.post("companyList", params, function (data) {
        wx.showLoading({
          title: '加载中',
        })
        var datas = data.dataList
           
        setTimeout(function () {
          wx.hideLoading();
          that.setData({
            logoArray: datas,
          }) 
        }, 500)    
      });
    }
  },
  chooseCompany:function(e){
    var that = this;
    var id = e.currentTarget.dataset.id;
    var name = e.currentTarget.dataset.name;
    var item = e.currentTarget.dataset.item;
    that.setData({
      id: id,
    })
    var companyStatus = wx.getStorageSync('companyStauts');
    if (companyStatus == 1){
      app.data.info.companyName = name;
      app.data.info.companyId = id;
      wx.setStorageSync('info', app.data.info);
      setTimeout(function () {
        wx.removeStorageSync('companyStauts');
        wx.navigateBack({
          url: '../Reg/Reg'
        })
      }, 1000)
    }
    else{
      if (app.data.info.token) {
        var params = {
          token: app.data.info.token,
          updateComId: that.data.id,
        };
        app.post("company", params, function (data) {
         wx.showModal({
            title: '',
            content: "1天后生效",
            showCancel: false, //不显示取消按钮
            confirmText: '确定',
            success:function(){
              wx.removeStorageSync('companyStauts');
              wx.navigateBack({
                url: '../personalData/personalData'
              })
            }
          }) 
          
         
          that.setData({
            companyId: that.data.id,
          })


          app.data.info.companyName = name;
          app.data.info.companyId = id;
          wx.setStorageSync('info', app.data.info);
        });
    }
    
    }
  },
 
})