const app = getApp()
Page({
  data: {
    searchIcon: "../../image/icon_search_@2x.png",
    hidden1: true,
    inputValue:'',
    qcArray:[],
    isNull:false,
  },

  onLoad: function (options) {

  },
  bindInput: function (e) {
    this.setData({
      inputValue: e.detail.value.trim(),
    })
    var inputValue = e.detail.value;  
    if (inputValue.length > 0) {
      this.setData({
        hidden1: false
      })
    } else {
      this.setData({
        hidden1: true,
      })
    }

  },
  /* 清空搜索内容 */
  clearInput: function (e) {
    this.setData({
      inputValue: '',
      hidden1: true,
      qcArray:'',
      isNull: false,
    })
  },
  // 搜索
  bindSearch: function (e) {
    var that = this;
    wx.showLoading({
      title: '加载中',
    })
    if (that.data.inputValue == ""){
      setTimeout(function () {
        wx.hideLoading()
      }, 0)
      wx.showModal({
        title: '提示',
        content: '请输入运单号或手机号搜索',
        success: function (res) {
          if (res.confirm) {
            console.log('用户点击确定')
          } else if (res.cancel) {
            console.log('用户点击取消')
          }
        }
      })
    }else{
      if (app.data.info.Phone != "0" || app.data.info.Phone != "" || app.data.info.Phone != null) {
        var params = {
          token: app.data.info.token,
          //phone: app.data.info.Phone,
          keyWord: that.data.inputValue
        };
        app.post("searchlist", params, function (data) {
          for (var i = 0; i < data.list.length; i++) {
            if (data.list[i].storePhone && data.list[i].customerPhone == "0") {
              data.list[i].storePhone = "--";
              data.list[i].customerPhone = "--";
            }
          }
          setTimeout(function () {
            wx.hideLoading()
          }, 1000)
          that.setData({
            qcArray: data.list,
          })
          console.log(that.data.qcArray)
          if (data.list.length == 0) {
            that.setData({
              isNull: true,
            })
          }
        });
      }
    }
    
      
      
  },
  rowClick:function(e){
    var params = {};
    params.token = app.data.token;
    params.expressId = e.currentTarget.dataset.oid;
    params.expressNum = e.currentTarget.dataset.epid;
    wx.setStorageSync('searchDetail', params);
    wx.navigateTo({
      url: '../searchDetail/searchDetail'
    })
  },
  // 撤回操作
  withdrawBtn: function(e) {
    var that = this;
    var id = e.currentTarget.dataset.id;
    var cacheIndex = e.currentTarget.dataset.index;
    console.log(id, cacheIndex)
    // return;
    that.setData({
      cacheIndex: cacheIndex,
      id: id,
      recordArray: this.data.recordArray,
    })
    wx.showModal({
      title: '撤回提醒',
      content: '您正在撤回此订单，确认撤回吗？',
      success: function(res) {
          if(res.confirm) {
            wx.showLoading({
              title: '请稍后',
              mask:true
            })
            let params = {
              token: app.data.info.token,
              batchId: that.data.id
            };
            app.post("cancelbatch", params, function (data) {
              that.data.qcArray[that.data.cacheIndex].stateCN = '已撤回';
              that.data.qcArray[that.data.cacheIndex].sendState = '2';
              that.setData({
                qcArray: that.data.qcArray
              })
              wx.hideLoading()
              wx.showToast({
                title: '撤回成功',
                icon: 'success',
                duration: 2000
              })
            }); 
          }else {

          }
      }
    })
  }
})