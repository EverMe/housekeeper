// pages/chestSend/chestSend.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    modules: [
      {
        img: '/image/newVersion/icon_touguijilu@2x.png',
        title: '投柜记录',
        text1: '已逾期：',
        text2: '待自提：',
        bind: 'toRecored'
      },
      {
        img: '/image/newVersion/icon_kuaidichaxun@2x.png',
        title: '快递查询',
        text1: '手机号查询',
        text2: '运单号查询',
        bind: 'toSearch'
      },
      {
        img: '/image/newVersion/icon_kongguichaxun@2x.png',
        title: '空柜查询',
        text1: '查询空柜状态',
        text2: '',
        bind: 'toEmpty'
      },
      {
        img: '/image/newVersion/icon_mimashezhi@2x.png',
        title: '密码设置',
        text1: '设置柜机登录密码',
        text2: '',
        bind: 'toSetpassword'
      },
      {
        img: '/image/newVersion/icon_wentijian@2x.png',
        title: '投递异常',
        text1: '投递异常处理',
        text2: '',
        bind: 'toAbn'
      }
    ],
    bannerImg: ['/image/newVersion/banner1.png'],
    numList: [],
    store:false,//默认是显示快递柜投递板块
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // return;
    if (options.module) {
      let modules = JSON.parse(options.module)
      this.setData({
        modules: modules,
        store: true
      })
      wx.setNavigationBarTitle({
        title: '门店投递',
      })
    }
    else {
      wx.setNavigationBarTitle({
        title: '快递柜投递',
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    if (!this.data.store) {
      // 请求各类订单数量
      let numList = [];
      app.post('newChestNum', { token: wx.getStorageSync('token') }, (data) => {
        numList.push(data.overdue)
        numList.push(data.waitTakeCount)
        numList.push(data.haveTakeCount)
        numList.push(data.exceptionCount)
        this.setData({
          numList: numList
        })
      })
    }

    app.post('overdueamount', { token: app.data.token }, (data)=> {
      this.setData({
        overNUM: data.amount,
      })
    })
  },

  // 跳转我的页面
  toUser: function () {
    wx.reLaunch({
      url: '/pages/user/user',
    })
  },

  // 跳转首页
  toHome: function () {
    wx.reLaunch({
      url: '/pages/index/index',
    })
  },

  // 跳转投柜记录
  toRecored: function () {
    wx.navigateTo({
      url: '/pages/ShowEvaluation/evaluate',
    })
  },

  // 跳转快递查询
  toSearch: function () {
    wx.navigateTo({
      url: '/pages/searchWaybill/waybill',
    })
  },

  // 跳转空柜查询
  toEmpty: function () {
    wx.navigateTo({
      url: '/pages/emptyChestSearch/search',
    })
  },

  // 跳转密码设置
  toSetpassword: function () {
    wx.navigateTo({
      url: '/pages/boxSet/boxSet',
    })
  },

  // 跳转门店投递
  toStore: function () {
    var params = { token: app.data.token, page: '', size: '', keyValue: '', 'type': 1 };
    app.post("batchlist", params, function (data) {
      if (data.data.length !== 0) {
        wx.navigateTo({
          url: '../problemList/problemList'
        })
      }
      else {
        wx.navigateTo({
          url: '../search/search'
        })
      }
    })
  },

  // 跳转投店记录
  toStoreRecord: function () {
    wx.navigateTo({
      url: '../deliveryRecord/deliveryRecord'
    })
  },

  // 跳转逾期记录
  toOverdue: function () {
    wx.navigateTo({
      url: '../Overdue/Overdue'
    })
  },

  // 跳转异常件
  toAbn: function() {
    wx.navigateTo({
      url: '/pages/abnExpress/abnExpress',
    })
  },
})