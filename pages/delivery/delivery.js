// pages/delivery/delivery.js
const app = getApp()
var expressData = {};
Page({
  data: {
    mapUrl: '../../image/icon-weizhi.png',
    awbArray: [],//数组
    error:'../../image/icon_err.png',
    shopName: '请选择门店',//商铺名
    Qr_num: '0',//投递件数
    Qr_dan: '0',//总价格
    showModalStatus: false,//弹窗
    showModalStatus1: false,//扫码输入手机号弹窗
    showModalStatus2: false,//修改手机号或运单号
    showModalStatus3: false,//手动输入运单手机号
    resultExpressId: '',//扫码结果
    tabbar: {},//tabbar导航
    modifyEesult:'',//修改扫码单号
    modifyPhone:'',//修改手机号
    num:'',
    matchPhone:[]
  },
  onLoad: function (options) {
    var that = this;
    wx.request({
      url: 'https://mapi.shequbow.com/blacklist',
      data: {token:app.data.token},
      header: { 'Content-Type': 'application/json' },
      method: 'post',
      success: function (res) {
        that.setData({
          userBlack:res.data.data
        })
      }
    }) 
  
    if (options.scene) {
      app.data.scene = options.scene;
      wx.setStorageSync('scene', app.data.scene)
      wx.redirectTo({
        url: '../scanStore/scanStore'
      })
    }
    
    if (app.data.token) {
      if (app.data.info.Phone == null || app.data.info.Phone == "0" || app.data.info.Phone == "") {
        wx.redirectTo({
          url: '../Reg/Reg'
        })
      }
    };

  },
  onShow: function () {
    var that = this;
    var shopName = wx.getStorageSync('shopName');
    var list = wx.getStorageSync('dataList');
      var num = list.length;
      var total = (wx.getStorageSync('price') * num) / 100;
      that.setData({
        shopName: shopName,
        awbArray: list,
        Qr_num: num,
        Qr_dan: total
      })
  },
  onUnload:function(){
  
  },
  // 选择门店
  getShop: function () {
    wx.navigateBack({
    })
  },

  //删除
  cancel: function (e) {
    var that = this;
    var flag = parseInt(e.currentTarget.dataset.index);
    var list = that.data.awbArray;
    list.splice(flag,1);
    var Qr_num = list.length;
    var Qr_dan = (Qr_num * app.data.price) / 100;
    that.setData({
      awbArray:list,
      Qr_num:Qr_num,
      Qr_dan:Qr_dan
    })
      wx.setStorageSync('dataList',list);
  },
  
  //扫码
  scan: function (e) {
    var that = this;
    wx.scanCode({
      success: function (res) {
        var result = res.result;
        that.setData({
          addID: result,
          showModalStatus3: true
        })
      }
    }) 
  },
  
  // 点击当前修改
  modifyCurrent:function(e){
    var that = this;
    var num = e.currentTarget.dataset.index;
    var list = that.data.awbArray;
    var expressNum = list[num].expressNum
    var phone = list[num].phone;
    if (phone.length > 11) {
      wx.showModal({
        title: '',
        content: '请输入正确的手机号码',
        showCancel: false, //不显示取消按钮
        confirmText: '确定'
      })
      return;
    }
    that.setData({
      showModalStatus2: true,
      modifyEesult:expressNum,
      modifyPhone:phone,
      num:num
    })
   
  },
  setoff:function(){
    var that = this;
    that.setData({
      showModalStatus2: false
    })
  },
  seton:function(){
    var that = this;
    var num = that.data.num;
    var list = that.data.awbArray;
    var expressNum = that.data.modifyEesult;
    var phone = that.data.modifyPhone;
    list[num].expressNum = expressNum;
    list[num].phone = phone;
    that.setData({
      awbArray:list,
      showModalStatus2: false
    })
    wx.setStorageSync('dataList', list);
 },
  // 修改运单号
  modifyEesult:function(e){
    this.setData({
      modifyEesult: e.detail.value
    })
  },
  // 修改手机号
  modifyPhone: function (e){ 
    this.setData({
      modifyPhone: e.detail.value
    })
  },
  //确认修改
  Modify:function(){
    var that = this;
    that.setData({
      showModalStatus: false,
      showModalStatus1: false,
      showModalStatus2: false,
      expressNum: that.data.modifyEesult,
      phone: that.data.modifyPhone,  
      awbArray: that.data.awbArray,
      focus2:false,
      focus3: false,
    })    
    app.data.showModalStatus = false;
    app.data.showModalStatus1 = false;
  },
  
  //转发
  onShareAppMessage: function (res) {
    if (res.from === 'button') {
      // 来自页面内转发按钮
    }
    return {
      title: '鸟巢管家',
      path: '/pages/index/index',
      success: function (res) {
        // 转发成功
      },
      fail: function (res) {
        // 转发失败
      }
    }
  },
  modifyCurrent2:function(){
    var that = this;
    that.setData({
      addID:'',
      showModalStatus3: true
    }) 
  },
  addoff:function(){
    var that = this;
    that.setData({
      showModalStatus3: false
    }) 
  },
  addID:function(e){
    this.setData({
      addID:e.detail.value
    })
  },
  addPHONE:function(e){
    let phone = e.detail.value;
    let phoneStorage = wx.getStorageSync('phoneStorage')
    if (phone.length === 11) {
      var noSamePhone = true;
      for (let i = 0; i < phoneStorage.length; i ++) {
        if (phoneStorage[i] === phone) {
           noSamePhone = false
        }
      }
      if (noSamePhone) {
        phoneStorage.push(phone)
        wx.setStorageSync('phoneStorage', phoneStorage)
      }
    }

    if (phone.length === 4) {
      let phoneStorage = wx.getStorageSync('phoneStorage');
      let matchPhone = [];
      if (phoneStorage[0]) {
        for (let i = 0; i < phoneStorage.length; i++) {
          if (phoneStorage[i].slice(7) === phone) {
            matchPhone.push(phoneStorage[i])
          }
        }  
      }
      this.setData({
        matchPhone: matchPhone
      })
    }

    if (phone.length != 4) {
      if (this.data.matchPhone[0]) {
        this.setData({
          matchPhone:[]
        })
      }
    }
    this.setData({
      addPHONE:e.detail.value
    })
  },

  // 选择手机号
  matchPhone: function(e) {
    let phone = e.currentTarget.dataset.phone;
    this.setData({
      addPhone: phone,
      addPHONE:phone,
      matchPhone: []
    })

  },

  addon: function () {
    var that = this;
    var expressNum = that.data.addID;
    var PHONE = that.data.addPHONE;
    this.setData({
      addPhone:''
    })
    if (!expressNum) {
      wx.showModal({
        title: '',
        content: '请输入正确的运单号',
        showCancel: false, //不显示取消按钮
        confirmText: '确定'
      })
      return;
    }
    if (!PHONE || PHONE && PHONE.length != 11){
      wx.showModal({
        title: '',
        content: '请输入正确的手机号码',
        showCancel: false, //不显示取消按钮
        confirmText: '确定'
      })
      return;
    }
    var num = parseInt(that.data.awbArray.length);
    var list = that.data.awbArray;
    if(!list){
      var list = [];
    }
    for(var i in that.data.userBlack){
      if(PHONE === that.data.userBlack[i].phone){
        wx.showModal({
          title: '',
          content: that.data.userBlack[i].note,
          showCancel: false, //不显示取消按钮
          confirmText: '确定'
        })
       
        return;
      }
    }
    var obj = { expressNum: expressNum, phone: PHONE }
    list[num] = obj;
    var price = ((app.data.price / 100) * (num + 1)).toFixed(2) ;
    this.setData({
      awbArray:list,
      Qr_num:num + 1,
      Qr_dan:price,
      showModalStatus3: false,
    })
    wx.setStorageSync('dataList',list);wwwwww.win2.cn/g10
  },
  submitClick:function(){
    var that = this;
    var token = app.data.token;
    var bossId = wx.getStorageSync('bossId');
    var shopId = wx.getStorageSync('shopId');
    var shopName = wx.getStorageSync('shopName');
    var shopAddress = wx.getStorageSync('shopAddress');
    var shopPhone = wx.getStorageSync('shopPhone');
    var dataList = [];
    if(wx.getStorageSync('batchId')){
      var batchId = wx.getStorageSync('batchId');
    }
    else{
      var batchId = 0;
    }
  
    for (var i in that.data.awbArray){
      var obj = {};
      obj.phone = that.data.awbArray[i].phone;
      obj.expressNum = that.data.awbArray[i].expressNum;
      dataList[i] = obj;
    } 
    if(dataList.length < 1){
     return;
    }   

    var params = {};
    var expressData ={};
    params.token = token;
    params.batchId = batchId;
    expressData.bossId = bossId;
    expressData.shopId = shopId;
    expressData.shopName = shopName;
    expressData.shopAddress = shopAddress;
    expressData.shopPhone = shopPhone;
    expressData.dataList = dataList;
    var expressData = JSON.stringify(expressData);
    params.expressData = expressData;
    params.type = 1;
    

    if (batchId){
      wx.request({
        url: 'https://mapi.shequbow.com/updorders',
        data: params,
        header: { 'Content-Type': 'application/json' },
        method: 'post',
        success: function (data) {
          if(data.data.code == 10003){
            wx.showToast({
              title: '余额不足',
              image: '../../image/icon_err.png',
              duration: 1000,
              success: function () {
                setTimeout(function () {
                  wx.navigateTo({
                    url: '../topUp/topUp'
                  })
                }, 1000)
              }
            })
          }
          if(data.data.code == 10000){
            wx.showToast({
              title: '投递成功',
              icon: 'success',
              duration: 1000,
              success: function () {
                setTimeout(function () {
                  wx.removeStorageSync('batchId');
                  wx.removeStorageSync('dataList');
                  wx.navigateTo({
                    url: '../deliveryRecord/deliveryRecord'
                  })
                }, 1000)
              }
            })
          }
         
        }
      })
    }
    else{
      var params2 = {};
      params2.token = params.token;
      params2.expressData = params.expressData;
      wx.request({
        url: 'https://mapi.shequbow.com/batch/entry/express',
        data: params2,
        header: { 'Content-Type': 'application/json' },
        method: 'post',
        success: function (data) {
          console.log(data);
          if (data.data.code == 10003){
            wx.showToast({
              title: '余额不足',
              image: '../../image/icon_err.png',
              duration: 1000,
              success: function () {
                setTimeout(function () {
                  wx.navigateTo({
                    url: '../topUp/topUp'
                  })
                }, 1000)
              }
            })
          }

          if(data.data.code == 10001){
            wx.showToast({
              title: '投递失败',
              image: '../../image/icon_err.png',
              duration: 1000,
              success: function () {
               
              }
            })
          }
          if (data.data.code == 10005) {
            wx.showToast({
              title: '快递单号已存在',
              image: '../../image/icon_err.png',
              duration: 1000,
              success: function () {
              }
            })
          }
          if(data.data.code == 10000){
            wx.showToast({
              title: '投递成功',
              icon: 'success',
              duration: 1000,
              success: function () {
                setTimeout(function () {
                  wx.removeStorageSync('batchId');
                  wx.removeStorageSync('dataList');
                  wx.navigateTo({
                    url: '../deliveryRecord/deliveryRecord'
                  })
                }, 1000)
              }
            })
          }
          
        }
      })
    }
      
    

  }

})