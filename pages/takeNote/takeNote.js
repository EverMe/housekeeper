const app = getApp()
Page({
  data: {
    searchIcon: "../../image/icon_search_@2x.png",
    hidden1: true,
    inputValue: '',
    qcArray: [],
    isNull: false,
  },

  onLoad: function (options) {

  },
  onShow:function(){
    var that = this;
    var params = {};
    params.token = app.data.token;
    app.post('getreturnrecord',params,function(data){
      that.setData({
        qcArray:data.list
      })
    })
  },
  bindInput: function (e) {
    this.setData({
      inputValue: e.detail.value.trim(),
    })
    var inputValue = e.detail.value;
    if (inputValue.length > 0) {
      this.setData({
        hidden1: false
      })
    } else {
      this.setData({
        hidden1: true,
      })
    }

  },
  /* 清空搜索内容 */
  clearInput: function (e) {
    this.setData({
      inputValue: '',
      hidden1: true,
      qcArray: '',
      isNull: false,
    })
  },
  // 搜索
  bindSearch: function (e) {
    var that = this;
    wx.showLoading({
      title: '加载中',
    })
    if (that.data.inputValue == "") {
      setTimeout(function () {
        wx.hideLoading()
      }, 0)
      wx.showModal({
        title: '提示',
        content: '请输入运单号或手机号搜索',
        success: function (res) {
          if (res.confirm) {
            console.log('用户点击确定')
          } else if (res.cancel) {
            console.log('用户点击取消')
          }
        }
      })
    } else {
      if (app.data.info.Phone != "0" || app.data.info.Phone != "" || app.data.info.Phone != null) {
        var params = {
          token: app.data.info.token,
          //phone: app.data.info.Phone,
          keyWord: that.data.inputValue
        };
        app.post("searchlist", params, function (data) {
          for (var i = 0; i < data.list.length; i++) {
            if (data.list[i].storePhone && data.list[i].customerPhone == "0") {
              data.list[i].storePhone = "--";
              data.list[i].customerPhone = "--";
            }
          }
          setTimeout(function () {
            wx.hideLoading()
          }, 1000)
          that.setData({
            qcArray: data.list,
          })
          if (data.list.length == 0) {
            that.setData({
              isNull: true,
            })
          }
        });
      }
    }



  },
  rowClick: function (e) {
    var params = {};
    params.token = app.data.token;
    params.expressId = e.currentTarget.dataset.oid;
    params.expressNum = e.currentTarget.dataset.epid;
    wx.setStorageSync('searchDetail', params);
    wx.navigateTo({
      url: '../searchDetail/searchDetail'
    })
  }
})