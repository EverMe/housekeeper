// pages/storeSet/storeSet.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    searchIcon: "../../image/icon_search_@2x.png",
    pic_nodata: '../../image/pic_nodata.png',
    list:[],
    page:'1',
    noMore: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getShoplist(1,20,'');
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },
  click:function(){
   
  },
  getShoplist:function(page,size,keyValue){
    var that = this;
    var token = app.data.token;
    var params = { token: token, page: page, size: size, keyValue: keyValue};
    app.post('authorizestorelist',params,function(res){
      var list = res.list;
      if (!list) {
        console.log('a');
        that.setData({
          noMore: true
        })
        return;
      }
      that.setData({
        noMore: false
      })
      for(var i in list){
        if(list[i].type == '1'){
          list[i].text = '取消';
          list[i].class = 'cancel'
        }
        else if(list[i].type == '0'){
          list[i].text = '授权';
          list[i].class = 'grant'
        }
      }
      that.setData({
          list:list
      })
      
    })
  },
  rowClick: function (e) {
    var that = this;
    var num = e.currentTarget.dataset.index;
    var rowtype = that.data.list[num].type;
    if(rowtype == 1){
      var rowtype = '0';
    }
    else if (rowtype == 0){
      var rowtype = '1';
    }
    var bossId = that.data.list[num].bossId;
    var params = {};
    params.token = app.data.token;
    params.type = rowtype;
    params.bossId = bossId;
    
    wx.showModal({
      content: '修改授权状态',
      confirmColor: '#007aff',
      cancelColor: '#007aff',
      confirmText: '是',
      cancelText: '否',
      success: function (res) {
        if (res.confirm) {
          app.post('authorizestore',params);
          wx.showToast({
            title: '修改成功',
            icon: 'success',
            duration: 2000,
            success: function () {
              setTimeout(function () {
                that.setData({
                  list: ''
                })
                that.getShoplist(1, 4, '');
              }, 2000) //延迟时间
            }
          })
         
        } else if (res.cancel) {
        
        }
      }
    })
  },
  bindInput:function(e){
    this.setData({
      keyValue: e.detail.value
    })
  },
  bindSearch:function(){
    var that  = this;
    that.setData({
      list:''
    })
    var val = that.data.keyValue;
    this.getShoplist(1, 20, val);
  }
})