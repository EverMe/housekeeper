const app = getApp()
Page({
  data: {
    selected: true,//待自提  
    selected1: false,//已自提
    selected2: false,//逾期
    selected3: false,//全部
    projectsArray: [],//列表
    batchData: {},
    totalCost: '',
    totalCount:'',
    waitTakeCount:'',
    haveTakeCount:'',
    overdue:'',
    haveReturnCount:'',
    phone: '',//手机
    tip: false,//true开启
    expressDataStr:{},
  },
  selected: function (e) {
    var that = this;
    that.setData({
      selected1: false,
      selected2: false,
      selected3: false,
      selected: true,
      projectsArray: app.data.waitTakeData,
    })
    if (app.data.waitTakeData.length == 0) {
      that.setData({
        tip: true
      })
    } else {
      that.setData({
        tip: false
      })
    }
  },
  selected1: function (e) {
    var that = this;
    that.setData({
      selected: false,
      selected2: false,
      selected3: false,
      selected1: true,
      projectsArray: app.data.haveTakeData,
    })
    if (app.data.haveTakeData.length == 0) {
      that.setData({
        tip: true
      })
    } else {
      that.setData({
        tip: false
      })
    }
  },
  selected2: function (e) {
    var that = this;
    this.setData({
      selected: false,
      selected1: false,
      selected3: false,
      selected2: true,
      projectsArray: app.data.overdueData,
    })
    if (app.data.overdueData.length == 0) {
      that.setData({
        tip: true
      })
    } else {
      that.setData({
        tip: false
      })
    }
  },
  selected3: function (e) {
    var that = this;
    this.setData({
      selected: false,
      selected1: false,
      selected2: false,
      selected3: true,
      projectsArray: app.data.returnData,
    })
    if (app.data.returnData.length == 0) {
      that.setData({
        tip: true
      })
    } else {
      that.setData({
        tip: false
      })
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    var batchItem = JSON.parse(options.item);   
    that.setData({
      batchData: batchItem,
    })
    console.log(that.data.batchData.batchId,)
  },
  onShow:function(){
    app.data.allData = '';
    app.data.overdueData = '';
    app.data.haveTakeData = '';
    app.data.waitTakeData = '';
    this.load();//请求一次投递中的所有订单
    this.batchnumder();//请求各类订单的数量
    
  },
  load: function (e) {
    var that = this;
    var params = {
      token: app.data.info.token,
      batchId: that.data.batchData.batchId,
      type: '0', 
    };
    app.post("batchid", params, function (data) {  
        console.log(data)
        var datas = data.dataList;
       if (datas == []){
          that.setData({
            tip: true,
          })
        }else if (datas != []){
            //app.data.allData = datas;
            //wx.setStorageSync('allData', datas)
            that.setData({
              tip: false,
            })
            for (var i = 0; i < datas.length; i++) {
              var overdueData = datas.filter((item) => {
                return item.state == 'OVERDUE'
              })
              app.data.overdueData = overdueData;
              wx.setStorageSync('overdueData', overdueData)
              if (overdueData){
                that.setData({
                  tip: false
                })  
              }else{
                that.setData({
                  tip: true
                })
              }
              var returnData = datas.filter((item) => {
                return item.state == 'RETURNED'
              })
              app.data.returnData = returnData;
              wx.setStorageSync('returnData', returnData)
              if (returnData) {
                that.setData({
                  tip: false
                })
              } else {
                that.setData({
                  tip: true
                })
              }
              var waitTakeData = datas.filter((item) => {
                return item.state == 'WAITTAKE' || item.state == 'OVERDUE'
              })
              
              app.data.waitTakeData = waitTakeData;
              wx.setStorageSync('waitTakeData', waitTakeData)

              var haveTakeData = datas.filter((item) => {
                return item.state == 'SIGNED'
              })
              app.data.haveTakeData = haveTakeData;
              wx.setStorageSync('haveTakeData', haveTakeData)
              
              if (datas[i].phone == 0) {
                that.setData({
                  phone: "--",
                })
              } else {
               that.setData({
                  phone: datas[i].phone
                })
              }
            }; 
          }         
        if (app.data.waitTakeData){
            that.setData({
              projectsArray: app.data.waitTakeData ,
              tip:false
            })
          }else{
                       that.setData({
              tip: true,
            })
          }
               
    });
  },
  // 订单数量
  batchnumder:function(){
    var that = this;
    var params = {
      token: app.data.info.token,
      batchId: that.data.batchData.batchId,
    };
    app.post("batchnumder", params, function (data) {
      var Cost = data.totalCost;
      console.log(data)
      that.totalCost = Cost * 0.01;
        that.setData({
          waitTakeCount: data.waitTakeCount,
          haveTakeCount: data.haveTakeCount,
          totalCount: data.totalCount,
          totalCost: that.totalCost.toFixed(2),
          overdue: data.overdue,
          haveReturnCount: data.haveReturnCount
        })
    });
  },
  getURL: function (e) {  
    var item = JSON.stringify(e.currentTarget.dataset.item)
    // console.log(item)
    wx.navigateTo({
      url: '../waybillDetails/waybillDetails?item=' + item
    })
  },
 

})