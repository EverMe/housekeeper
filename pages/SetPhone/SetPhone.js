// pages/SetPhone/SetPhone.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    phone:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    wx.getStorage({
      key: 'setDetaildata',
      success: function (res) {
        that.setData({
          list: res.data
        })

      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },
  phoneInput: function (e) {
    this.setData({
      phone: e.detail.value
    })
  },
  setClick:function(){
    var params = {};
    params.token = app.data.token;
    params.phone = this.data.phone;
    params.expressId = this.data.list.id;
    app.post("updphone", params, function (res) {
      wx.showToast({
        title: '修改成功',
        icon: 'success',
        duration: 1000,
        success: function () {
          setTimeout(function () {
            wx.navigateTo({
              url: '../Overdue/Overdue'
            })
          }, 1000)
        }
      })

    })
  }
})