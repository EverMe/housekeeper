const app = getApp()
var time = require("../../utils/util.js");
var url = "https://miniapp.shequbow.com/get/shop/info";
Page({
  data: {
    shopName:'',
    shopAddress:'',
    shopPhone:'',
    createDate:'',
    shopId:'',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    var shopData = JSON.parse(options.item);
    if (app.data.info.Phone) {
      var params = {
        token: app.data.info.token,
        shopId: shopData.shopId
      };
      app.post("shoplist", params, function (data) {
        console.log(data)
        that.setData({
          shopName: data.shopInfo.shopName,
          shopAddress: data.shopInfo.shopAddress,
          shopPhone: data.shopInfo.shopPhone,
          createDate: time.formatTime(data.shopInfo.createDate, 'Y-M-D h:m:s'),
        })
      });
    } 
  },
})