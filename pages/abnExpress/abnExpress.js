// pages/abnExpress/abnExpress.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
  
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    app.post('abnExpress', { token: wx.getStorageSync('token') }, (data) => {
      this.setData({
        abnList: data.list,
      })
    })
  },

  toDetail: function(e) {
    let detail = e.currentTarget.dataset.detail;
    wx.navigateTo({
      url: '/pages/waybillDetail/detail?detail='+JSON.stringify(detail),
    })
  },
})