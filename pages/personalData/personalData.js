const app = getApp()
var time = require("../../utils/util.js");
Page({
  data: {
    presonalData:[],
    idNumber:'',
    showModalStatus:false,//弹窗
  },
  onLoad: function (options) {  
      
  },
 onShow:function(){
   this.info();  
   
 },

  //个人资料
  info:function(){
    var that = this;
    if (app.data.info.Phone) {
      var params = {
        token: app.data.info.token,
      };
      app.post("info", params, function (data) {
        wx.showLoading({
          title: '加载中',
        })
        for (var i = 0; i < data.length; i++) {
          var createDate = time.formatTime(data[0].createDate, 'Y-M-D h:m:s')
        } 
        setTimeout(function () {
          wx.hideLoading();
          that.setData({
            presonalData: data,
            createDate: createDate,
            idNumber: data[0].idNumber
          })
        }) 
        
           
      });
    }
  },
 //选择公司
  toCompany:function(){
    wx.navigateTo({
      url: '../company/company'
    })
  },
  //弹窗
  powerDrawer: function (e) {
    this.setData({
      showModalStatus: false,
    });
  },
  //点击修改身份证号码
  bindIdcrad: function (e) {
    var that =  this;
    that.setData({
      showModalStatus: true,
      id_crad: that.data.presonalData[0].idNumber,
    });
  },
  // 取消弹窗
  formReset:function(){
    this.setData({
      showModalStatus: false,
    });
  },

  // 获取身份证号
  Idcard: function (e) {
    this.setData({
      id_crad: e.detail.value,
    });
  },
  // 确认修改身份证
  idName: function (e) {
    var that = this;
    if (app.data.info.Phone) {
      var params = {
        token: app.data.info.token,
        idNumber: that.data.id_crad,
      };
      var regIdNo = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
      if (!regIdNo.test(that.data.id_crad)) {
        wx.showToast({
          title: '身份证格式有误',
          duration: 2000,
          image: "/image/icon_delete_@2x.png"
        })
        // that.setData({
        //   showModalStatus: true,
        // })
        return;
      }
      app.post("userid", params, function (data) {
       
       
          that.setData({
            showModalStatus: false,
            idNumber: that.data.id_crad
          })
          wx.showToast({
            title: '修改成功',
          })
   
        app.data.info.idNumber = that.data.id_crad
        wx.setStorageSync('info', app.data.info)
      });
    }
    
  },
})