// pages/OverdueDetail/OverdueDetail.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
  
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    wx.getStorage({
      key: 'orderDATA',
      success: function (res) {
        that.setData({
          list: res.data
        })
      
      }
    })
   
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    wx.removeStorageSync('orderDATA') 
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },
  setClick:function(){
    var obj = {};
    var that = this;
    obj.express_name = that.data.list.expressName;
    obj.express_id = that.data.list.express_id;
    obj.phone = that.data.list.phone;
    obj.box = that.data.list.box;
    obj.id = that.data.list.id;
    wx.setStorageSync('setDetaildata', obj);
    wx.navigateTo({
      url: '../Setdetail/Setdetail'
    })
  }
})