// pages/Overdue/Overdue.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    searchIcon: "../../image/icon_search_@2x.png",
    List:[],
    imageSrc: '', pic_nodata: '../../image/pic_nodata.png',
    noMore: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    var params = {
      token: app.data.info.token
    };

    
    app.post("companyList", params, function (data) {
      that.setData({
        comList: data.dataList
      })
      app.post('getoverdue', params, function (data) {
        var overdue = data;
        console.log(overdue);
        if (!overdue.length){
         that.setData({
            noMore: true
          })
          return;
        }
        that.setData({
          noMore: false
        })
        for(var i in overdue){
          if (Number(overdue[i].express_state) === 0){
            overdue[i].stateTEXT = '入库交接'
          }
          if (Number(overdue[i].express_state) === 1) {
            overdue[i].stateTEXT = '签收'
          }
          if (Number(overdue[i].express_state) === 4) {
            overdue[i].stateTEXT = '待处理'
          }
          if (Number(overdue[i].express_state) === 2) {
            overdue[i].stateTEXT = '推荐'
          }
          if (Number(overdue[i].express_state) === 7) {
            overdue[i].stateTEXT = '销件'
          }
          if (Number(overdue[i].express_state) === 9) {
            overdue[i].stateTEXT = '核单确认'
          }
          overdue[i].overdueTEXT = '(逾期' + overdue[i].overdue+'天)'
        }
        that.setData({
          List: overdue
        })
        var List = that.data.List;
        var comList = that.data.comList;
        var array = [];
        for(var i in List){
         
          for(var j in comList){
            if (Number(List[i].com_id) === Number(comList[j].id)){
              List[i].src = '../../image/' + comList[j].id+'.png';
              List[i].expressName = comList[j].name;
            }
          }
        }
        that.setData({
          List: List
        })

      })
     
    });

    
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },
  bindSearch:function(){
    wx.showLoading({
      title: '加载中',
    })
    setTimeout(function () {
      wx.hideLoading()
    }, 500)
  },
  rowClick:function(e){
    var num = e.currentTarget.dataset.index;
    var data = this.data.List[num];
    wx.setStorageSync('orderDATA', data); 
    wx.navigateTo({
      url: '../OverdueDetail/OverdueDetail'
    })
  }
  
})