// pages/waybillDetail/detail.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    openSuccess:false,
    recall:true,
    phoneFocus:true,
    reSendStatus:false,
    reSendText:'重发取件码'
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let detail = JSON.parse(options.detail);
    let expressNum = detail.expressNum;
    if (options.backData) {
      let backData = JSON.parse(options.backData)
      this.setData({
        backData: backData
      })
    }

    let params = {
      token: wx.getStorageSync('token'),
      expressNum: expressNum
    }

    app.post('msgcallback', params, (data)=> {
      console.log(data.data.error_info)
      this.setData({
        msgDetail: data.data
      })
    })
   
    // 处理通知状态
    if (detail.msgType == '0') {
      if (detail.msgStatus == '0') {
        detail['inform'] = '短信发送成功'
      }
      else if (detail.msgStatus == '1') {
        detail['inform'] = '短信已送达'
      }
      else if (detail.msgStatus == '2') {
        detail['inform'] = '短信发送失败'
      }
    }
    else if (detail.msgType == '1') {
      detail['inform'] = '微信已推送'
    }
   
    this.setData({
      detail: detail,
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  // 弹出修改手机model
  updatePhone: function (e) {
    var phone = e.currentTarget.dataset.phone;
    var expressid = e.currentTarget.dataset.expressid;
    var bigindex = e.currentTarget.dataset.bigindex;
    var index = e.currentTarget.dataset.smindex;

    this.setData({
      PhoneModel: true,
      needSetphone: phone,
      getexpressId: expressid,
      bigindex: bigindex,
      smindex: index,
    })

  },

  // 重发短信取件码
  rsendNote: function () {
    if (this.data.reSendText!='重发取件码') {
      wx.showToast({
        title: '1分钟只能发送一次取件码',
        icon:'none',
        duration:2000
      })
      return;
    }
    this.setData({
      reSendStatus: true,
    })
    
  },

  // 确定发送短信
  sureSend: function () {
    this.setData({
      reSendStatus: false
    })
    app.post('resendmsg', { token: wx.getStorageSync('token'), expressId: this.data.detail.expressId }, (data) => {

      wx.showToast({
        title: '发送成功',
      })
      let timeOut = 60;
      var that = this;
      var timeInterval = setInterval(function () {
        timeOut--;
        console.log(timeOut)
        that.setData({
          reSendText: timeOut + 's'
        })
        if (timeOut == 1) {
          clearInterval(timeInterval);
          that.setData({
            reSendText: '重发取件码'
          })
        }
      }, 1000)
      
    })
  },

  // 取消重发短信
  cancleSend: function () {
    this.setData({
      reSendStatus: false
    })
  },

  // 撤回
  callback: function () {
  
    wx.showModal({
      title: '开柜撤件提醒',
      content: '您即将打开格口并撤回此快件，请您务必确定位于柜机现场，请确定是否开柜撤件？',
      cancelText: '取消',
      cancelColor: '#A8A8A8',
      confirmText: '确定开柜',
      confirmColor: '#EF5D0F',
      success: (res) => {
        if (res.confirm) {
          wx.showLoading({
            title: '请稍后',
            mask:true
          })
          app.post('cancelbatch', { token: wx.getStorageSync('token'), batchId: this.data.detail.batchId }, (data) => {
            wx.hideLoading()

            this.data.detail.stateCN = '已撤回';
            this.data.detail.state = 'SIGNED';
            
            this.setData({
              openSuccess: true,
              detail:this.data.detail
            })
            wx.setStorageSync('chehui', true)
          })
        }
      }
    })


  },

  // 关闭模态窗
  closeMengban: function () {
    this.setData({
      openSuccess: false,
    })
  },

  // 拨打电话
  call: function(e) {
let phone = e.currentTarget.dataset.phone;
wx.makePhoneCall({
  phoneNumber: phone,
})
  },

  // 修改手机号
  modPhone: function(e) {
    let phone = e.currentTarget.dataset.phone;
    this.setData({
      PhoneModel:true,
      needSetphone:this.data.detail.phone
    })
  },

  // 暂不修改
  closePhoneModel: function () {
    this.setData({
      PhoneModel: false
    })
  },

  // 确认修改手机号

  sureSet: function () {
    var that = this;
    if (this.data.sureSetPhone) {

      app.post('updphone', { token: wx.getStorageSync('token'), expressId: that.data.detail.expressId, phone: that.data.phoneNumber }, (data) => {
        this.data.detail.phone = that.data.phoneNumber;
        this.setData({
          PhoneModel: false,
          detail: this.data.detail,
        })
        wx.showToast({
          title: '修改成功',
        })
        wx.setStorageSync('backData', this.data.backData)
        wx.setStorageSync('backPhone', this.data.phoneNumber)
      })
    }

  },

  // 修改手机的监听事件
  inputBind: function (e) {
    let phone = e.detail.value;
    if (phone.length == 11) {
      this.setData({
        phoneNumber: phone,
        sureSetPhone: true,
        firstReset: false
      })
    }
    else {
      this.setData({
        sureSetPhone: false,
        firstReset: false
      })
    }
  },

  // 删除输入的手机号
  resetPhone: function () {
    this.setData({
      sureSetPhone: false,
      needSetphone: '',
      firstReset: false,
      phoneFocus: true,
    })
  },
})