const app = getApp()
var time = require("../../utils/util.js");
Page({
  data: {
    nickname:'',//昵称
    phone:'',//手机号
    balance:'0',//余额
    page:'1',
    size:'150',
    tabbar: {}
  },
  onLoad:function(options){
    // app.editTabBar(); 
  },
  onShow:function(){
    var that = this;
    that.balance();
    that.setData({
      nickname: app.data.info.nickname,
      phone:app.data.info.Phone
    })
  },
  // 获取余额
  balance: function (e) {
    var that = this;
    if (app.data.info.Phone != "0" || app.data.info.Phone != "" || app.data.info.Phone != null) {
      var params = { 
        token: app.data.info.token, 
      };
      app.post("balance", params, function (data) {
        that.setData({
          balance: data.balance / 100
        })
      });
    }
  },
  scan:function(){
    var that = this;
    wx.scanCode({
      success: function (res) {
        var result = /^[0-9A-Za-z/-]{8,32}$/;
        if (result.test(res.result)) {
          wx.redirectTo({
            url: '/pages/index/index'
          })
          app.data.showModalStatus = true;
          app.data.showModalStatus1 = true;

          app.data.resultExpressId = res.result;
          wx.setStorageSync('resultExpressId', app.data.resultExpressId)
          
        } else {
          wx.showToast({
            title: "扫码失败",
          })
        }
      }, fail: function (res) {
        wx.showToast({
          title: '失败',
          icon: 'success',
          duration: 2000
        })
      },
    }) 
  },
  getpers:function(){
    wx.navigateTo({
      url: '../personalData/personalData'
    })
  },
  getDeliveryRecord: function () {
    wx.navigateTo({
      url: '../takeNote/takeNote'
    })
  },
  getPrepaid: function () {
    wx.navigateTo({
      url: '../Prepaid/Prepaid'
    })
  },
  storeSet:function(){
    wx.navigateTo({
      url: '../storeSet/storeSet'
    })
  },
  boxSet:function(){
    wx.navigateTo({
      url: '../boxSet/boxSet'
    })
  },

  // 跳转首页
  toHome: function(){
    wx.redirectTo({
      url: '/pages/index/index',
    })
  },
})