// pages/emptyChestSearch/search.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    keyWord:null,
    noData:false,
    chestList:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    app.post('emptyChestSearch', { token: wx.getStorageSync('token')}, (data) => {
      this.setData({
        chestList: data.list
      })
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  // 监听输入框
  keyWord: function(e) {
    let value = e.detail.value;
    this.setData({
      keyWord:value
    })
  },

  // 清空输入内容
  reset: function() {
    this.setData({
      keyWordValue:'',
      keyWord:''
    })
  },

  // 查询空柜
  search: function() {
    app.post('emptyChestSearch', { token: wx.getStorageSync('token'), keyValue:this.data.keyWord},(data)=>{
      // if(!data.list[0]) {
      //   this.setData({
      //     noData:true
      //   })
      // }
      this.setData({
        chestList:data.list
      })
    })
  },

  // 授权柜子
  setcabinets: function(e) {
    let index = e.currentTarget.dataset.index;
    let cabid = e.currentTarget.dataset.cabid;
    let status = e.currentTarget.dataset.status;

    app.post('setcabinets', { token: wx.getStorageSync('token'), cabId: cabid, type: status},(data)=>{
      if (this.data.chestList[index].type == '0') {
        this.data.chestList[index].type = '1';
        wx.showToast({
          title: '收藏成功',
          mask:true,
        })
      }
      else {
        this.data.chestList[index].type = '0';
        wx.showToast({
          title: '取消成功',
          mask: true,
        })
      }
      
      this.setData({
        chestList: this.data.chestList
      })
    })
  },
})