const app = getApp()

Page({

  data: {
    searchIcon: "../../image/icon_search_@2x.png",//搜索
    QrcodeIcon: "../../image/icon_search_scan_@2x.png",//扫描
    hidden1:true,
    inputValue:'',//输入框
    hiddenLoading: true, 
    listArray:[],//列表
    delBtnWidth: 140,//删除按钮宽度单位（rpx）
  },

  onLoad: function (options) {
    var that = this;
    that.loadMore();
    that.initEleWidth();
    var params = {
      token: app.data.info.token,
    };
    app.post("historystore", params, function (data) {
      that.setData({
        listArray: data.dataList,
      })


    }); 
  },
  //拉取列表
  loadMore:function(e){
    var that = this;   
    if (app.data.info.Phone != "0" || app.data.info.Phone != "" || app.data.info.Phone != null) {  
      var params = {
        token: app.data.info.token,
        keyValue: that.data.inputValue,
      };
      app.post("searchShop", params, function (data) {
          that.setData({
            listArray: data.dataList||""
          })
      });   
    }
  },
  // 获取输入框的值
  bindInput: function (e) {
   this.setData({
      inputValue: e.detail.value.trim(),
    })
   
  },
/* 清空搜索内容 */
  clearInput:function(e){
    this.setData({
      inputValue : '',
      hidden1: true,
      hidden: false,
      listArray:[]
    })
    this.loadMore();

    
  },
  // 点击选中并跳转
  readDetail: function (e) {
    console.log(e);
    var id = e.currentTarget.dataset.index,
      index = parseInt(e.currentTarget.dataset.index),
      num = parseInt(e.currentTarget.dataset.index),
      name = e.currentTarget.dataset.item.shopName,
      shopid = e.currentTarget.dataset.item.shopId,
      address = e.currentTarget.dataset.item.shopAddress,
      shopphone = e.currentTarget.dataset.item.shopPhone,
      price = e.currentTarget.dataset.item.price,
      bossId= e.currentTarget.dataset.item.boss_id
    this.curIndex = parseInt(e.currentTarget.dataset.index)
    var that = this
   
    var item = JSON.stringify(e.currentTarget.dataset.item)
    app.data.shopName = name;
    wx.setStorageSync('shopName', name)

    app.data.shopId = shopid;
    wx.setStorageSync('shopId', shopid)

    app.data.shopAddress = address;
    wx.setStorageSync('shopAddress', address)

    app.data.shopPhone = shopphone;
    wx.setStorageSync('shopPhone', shopphone)

    app.data.price = price;
    wx.setStorageSync('price', price)
    
    app.data.bossId = bossId;
    wx.setStorageSync('bossId', bossId)
    
    wx.navigateTo({
      url: '../delivery/delivery'
    })
    
    this.setData({
      item: item,
      curNavId: id,
      curIndex: index,
      num: index,
      boolean: true,
    })

  },
  //搜索
  bindCancel: function (e) {
    this.loadMore();
   
  },
  //左滑删除
  touchS: function (e) {
    if (e.touches.length == 1) {
      this.setData({
        startX: e.touches[0].clientX
      });
    }
  },

  touchM: function (e) {
    if (e.touches.length == 1) {
      var moveX = e.touches[0].clientX;
      var disX = this.data.startX - moveX;
      var delBtnWidth = this.data.delBtnWidth;
      var txtStyle = "";
      if (disX == 0 || disX < 0) {//如果移动距离小于等于0，文本层位置不变
        txtStyle = "left:0px";
      } else if(disX > 0){//移动距离大于0，文本层left值等于手指移动距离
        txtStyle = "left:-" + disX + "px";
        if (disX >= delBtnWidth) {
          txtStyle = "left:-" + delBtnWidth + "px";
        }
      }
      var index = e.currentTarget.dataset.index;      
      var listArray = this.data.listArray;
      listArray[index].txtStyle = txtStyle;
      this.setData({
        listArray: listArray
      });
    }

  },

  touchE: function (e) {
    if (e.changedTouches.length == 1) {
      //手指移动结束后水平位置
      var endX = e.changedTouches[0].clientX;
      //触摸开始与结束，手指移动的距离
      var disX = this.data.startX - endX;
      var delBtnWidth = this.data.delBtnWidth;
      //如果距离小于删除按钮的1/2，不显示删除按钮
      var txtStyle = disX > delBtnWidth / 2 ? "left:-" + delBtnWidth + "px" : "left:0px";
      var index = e.currentTarget.dataset.index;
      var listArray = this.data.listArray||"";
      listArray[index].txtStyle = txtStyle;
      this.setData({
        listArray: listArray
      });
    }
  },
  //获取元素自适应后的实际宽度
  getEleWidth: function (w) {
    var real = 0;
    try {
      var res = wx.getSystemInfoSync().windowWidth;
      var scale = (750 / 2) / (w / 2);
      real = Math.floor(res / scale);
      return real;
    } catch (e) {
      return false;
    }

  },
//删除按钮
  initEleWidth: function () {
    var delBtnWidth = this.getEleWidth(this.data.delBtnWidth);
    this.setData({
      delBtnWidth: delBtnWidth
    });
  },

  //点击删除按钮
  delItem: function (e) {
    var index = e.currentTarget.dataset.index;
    var listArray = this.data.listArray;
    listArray.splice(index, 1);
    this.setData({
      listArray: listArray
    });

  },
})