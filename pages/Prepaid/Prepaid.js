const app = getApp()
var time = require("../../utils/util.js");
Page({
  data: {
    records: [],//流水列表
    page: '1',//页数
    size: '10',//条数
    hasMore: true,//加载中/没有更多内容
    noMore: false,
    pic_nodata: '../../image/pic_nodata.png',
  },

  onLoad: function (options) {
  
  },
  onShow: function () {
    var that = this;
    that.setData({
      records: [],
      page: 1
    })
    that.Prepaid();
  },
  // 流水列表
  Prepaid: function (e) {
    var that = this;
    if (app.data.info.Phone == "" || app.data.info.Phone == "0" || app.data.info.Phone == null) {
      
      that.setData({
        hasMore: false,
        noMore: true,
      })
    } else {
      var params = {
        token: app.data.info.token,
        page: that.data.page,
        size: that.data.size,
      };
      app.post("userlist", params, function (data) {  
        console.log(data);     
        wx.showLoading({
          title: '加载中',
        })
        var datas = data.data;
        if (data.data.length == "0") {
          that.setData({
            hasMore: false,
            noMore: true,
          })
          setTimeout(function () {
            wx.hideLoading()
          }, 1000)
        } else {
          for (var i = 0; i < datas.length; i++) {
            var num = Number(datas[i].chargeVal) / 100
            datas[i].chargeVal = num;
          }
          for (var i = 0; i < datas.length; i++) {
            if (datas[i].type == 0) {
              datas[i].type = '充值';
              datas[i].chargeVal = '＋' + datas[i].chargeVal;
              datas[i].textcolor = 'orange';
            } else if (datas[i].type == 1) {
              datas[i].type = '支出';
              datas[i].chargeVal = '－' + datas[i].chargeVal;
            } else if (datas[i].type == 2) {
              datas[i].type = '退款';
              datas[i].chargeVal = '＋' + datas[i].chargeVal;
              datas[i].textcolor = 'orange';
            }
          
          }
          
          
          //时间转换
          for (var i = 0; i < datas.length; i++) {
            datas[i].createDate = time.formatTime(datas[i].createDate, 'Y-M-D h:m:s')
            
          }
          var datasArr = that.data.records.concat(datas)
          setTimeout(function () {
            wx.hideLoading();
            that.setData({
              records: datasArr,
              hasMore: false,
              noMore: false,
            })
          })
          
        }
      });
    }

  },
  // 下拉刷新
  onPullDownRefresh: function () {
    wx.showNavigationBarLoading(); 
    this.setData({
      records: [],
      page: 1,
      hasMore:true
    })
    setTimeout(() => {
      this.Prepaid()
      wx.hideNavigationBarLoading();
      // 停止下拉动作  
      wx.stopPullDownRefresh();
      
    }, 200)
   
  },
  // 上拉加载
  onReachBottom: function () {
    var that = this;
    if (that.data.records.length >= that.data.size * that.data.page) {
      setTimeout(() => {
        that.data.page++
        that.Prepaid();
        this.setData({
          hasMore: true,
          noMore: false,
        })
      }, 200)
    }
   
  },  
})