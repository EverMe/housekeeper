const app = getApp()
var time = require("../../utils/util.js");

Page({
  data: {
    searchIcon: "../../image/icon_search_@2x.png",//搜索图标
    QrcodeIcon: "../../image/icon_search_scan_@2x.png",//取消搜索图标
    pic_nodata: '../../image/pic_nodata.png',
    recordArray: [],//数组
    page: '1',//页数
    size: '5',//条数
    hidden1: true,//取消搜索
    inputValue: '',//搜索框
    sendState: '',//状态
    createDate: [],//时间
    batchId: '',//批次ID
    hasMore: true,//加载中/没有更多内容
    showModalStatus: false,//
    noMore: false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },
  onShow: function (e) {
    var that = this;
    that.setData({
      recordArray: [],
      page: 1
    })
    that.LoadRec();
  },
  LoadRec: function (e) {
    var that = this;    
    if (app.data.info.Phone == "" || app.data.info.Phone == "0" || app.data.info.Phone == null) {
      that.setData({
        hasMore: false,
        noMore: true,
      })
    } else { 
      var params = {
        token: app.data.info.token,
        page: that.data.page,
        size: that.data.size,
        type:0,
        keyValue: that.data.inputValue,
      };
      app.post("batchlist", params, function (data) {
       if (data == "") {
          that.setData({
            hasMore: false,
            noMore: true,
          })
        } else {
          var createDateArr = [];
          var datas = data.data;
          for (var i = 0; i < datas.length; i++) {
            if (datas[i].sendState == 0) {
              datas[i].sendState = '待接收';
            } else if (datas[i].sendState == 1) {
              datas[i].sendState = '已接收';
            } else if (datas[i].sendState == 2) {
              datas[i].sendState = '已撤回';
            } else if (datas[i].sendState == 3) {
              datas[i].sendState = '已拒收';
            } else if (datas[i].sendState == 5) {
              datas[i].sendState = '待自提';
            } else if (datas[i].sendState == 6) {
              datas[i].sendState = '已取件';
            }
            createDateArr: datas

          }
          // // 价格换算
          for (var i = 0; i < datas.length; i++) {
            datas[i].cost = (datas[i].cost / 100);

          }
          //时间转换
          for (var i = 0; i < datas.length; i++) {
            datas[i].createDate = time.formatTime(datas[i].createDate, 'Y-M-D h:m:s')
            createDateArr = datas
          }
          var datasArr = that.data.recordArray.concat(createDateArr)
            that.setData({
              recordArray: datasArr,
              hasMore: false,
              noMore: false,
            })
        //  console.log(that.data.recordArray)          
        }
        
      });
    }
    
  },

  //获取输入框值
  bindInput: function (e) {
    var that = this
    that.setData({
      inputValue: e.detail.value.trim(),
      page: '1',
      recordArray: [],
      hasMore: false
    })
    var inputValue = e.detail.value
    if (inputValue.length >= 2) {
      wx.showLoading({
        title: '加载中',
      })
      setTimeout(function () {
        wx.hideLoading()
      }, 1000)
      this.onShow();
    } else if (inputValue.length == 0) {
      this.onShow();
    } else if (inputValue.length > 0) {
      that.setData({
        hidden1: false
      })
    } else {
      that.setData({
        hidden1: true,
      })
    }
  },
  // 搜索
  search: function () {
    var that = this
    that.setData({
      page: '1',
      recordArray: [],
      hasMore: false
    })
    this.onShow();
  },
  // 弹窗
  powerDrawer: function () {
    this.setData({
      showModalStatus: false,
    })
  },
  formReset: function () {
    this.setData({
      showModalStatus: false,
    })
  },
  //撤回
  cancelRec: function (e) {
    var that = this;
    var id = e.currentTarget.dataset.id;
    that.setData({
      cacheIndex: e.currentTarget.dataset.index,
      id: id,
      recordArray: this.data.recordArray,
      showModalStatus: true,
    })
  },
  withdraw: function (e) {
    var that = this;
    that.setData({
      showModalStatus: false,
    })
    var params = {
      token: app.data.info.token,
      batchId: that.data.id
    };
    app.post("cancelbatch", params, function (data) {
      that.data.recordArray[that.data.cacheIndex].sendState = '已撤回'
      that.setData({
        recordArray: that.data.recordArray
      })
      wx.showToast({
        title: '撤回成功',
        icon: 'success',
        duration: 2000
      })
    });
  },
  //跳转门店详情
  getShop: function (e) {
    var item = JSON.stringify(e.currentTarget.dataset.item)
    // console.log(item)
    wx.navigateTo({
      url: '../storesDetails/storesDetails?item=' + item
    })
  },
  //跳转投递订单
  getPro: function (e) {
    var item = JSON.stringify(e.currentTarget.dataset.item)
    wx.navigateTo({
      // url: '../ShowEvaluation/evaluate?item=' + item
      url: '../ProcessingProjects/ProcessingProjects?item=' + item
      
    })
  },
  //清除搜索内容
  clearInput: function (e) {
    this.setData({
      inputValue: '',
      hidden1: true,
    })
    this.onShow();
  },
  // 下拉刷新
  onPullDownRefresh: function () {
    wx.showNavigationBarLoading();
    this.setData({
      recordArray: [],
      page: 1,
      hasMore: true,
      noMore: false,
    })
    setTimeout(() => {
      this.LoadRec();
      wx.hideNavigationBarLoading();
      // 停止下拉动作  
      wx.stopPullDownRefresh();

    }, 200)

  },
  //加载更多
  onReachBottom: function () {
    var that = this;
    //  console.log(that.data.recordArray.length)
    if (that.data.recordArray.length >= that.data.size * that.data.page) {
      setTimeout(() => {
        that.data.page++
        that.LoadRec();
        this.setData({
          hasMore: true,
          noMore: false,
        })
      }, 200)
    } else {
      this.setData({
        hasMore: false,
        noMore: false,
      })
    }
  },
  searchClick:function(){
    wx.navigateTo({
      url: '../queryCourier/queryCourier'
    })
  }
})