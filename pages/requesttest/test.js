// pages/requesttest/test.js
// var app = getApp()
// import {HTTP} from '../../http.js'
// var http = new HTTP()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    change: false
  },

  getsms: function () {
    // var tmp = new Date();
    // function f() {
    //   console.log(tmp)
    //   var tmp = 'hellow'
    // }
    // f()
    // var s = 'hellow';
    // for(var i = 0; i < s.length; i ++) {
    //   console.log(i)
    // }
    // console.log(i)

    // function f1() {
    //   let n = 5;
    //   if (true) {
    //    n = 10;
    //   }
    //   console.log(n); // 5
    // }
    // f1()

    // {
    //   let es6 = 'ok';

    // }

    // function f() { console.log('I am outside!'); }

    // (function () {
    //   if (false) {
    //     // 重复声明一次函数f
    //     function f() { console.log('I am inside!'); }
    //   }

    //   f();
    // }());


    //   let a = 'secret';
    //   let f = function () {
    //     return a;
    //   };
    //   f()
    // console.log(f())

    // const a = [];
    // a['b'] = 1;
    // console.log(a)
    // var constantize = (obj) => {
    //   Object.freeze(obj);
    //   console.log(Object.keys(obj))
    //   Object.keys(obj).forEach((key, i) => {
    //     if (typeof obj[key] === 'object') {
    //       constantize(obj[key]);
    //     }
    //   });
    // };



    // const foo = Object.freeze({a:{b:{c:1}}});

    // constantize(foo)
    // // 常规模式时，下面一行不起作用；
    // // 严格模式时，该行会报错
    // foo.a.b = 123;
    // console.log(foo)

    // var a = 1;
    // var b = 2;

    // var global = function() {

    //   if (a == 1) {
    //     return a=b;
    //   }

    //   if (b == 2) {
    //     return b=3;
    //   }
    // }
    // console.log(global())

    // function fibs() {
    //   let a = 0;
    //   let b = 1;
    //   while (true) {
    //     yield a;
    //     [a, b] = [b, a + b];
    //   }
    // }

    // let [first, second, third, fourth, fifth, sixth] = fibs();
    // first

  },

  change: function () {
    this.setData({
      change: !this.data.change
    })
  },



  // data: [
  //   {
  //     list: [
  //       {

  //       },
  //       {

  //       }
  //     ],
  //     row: {
  //       listSum:99,
  //       location: '华寓001'
  //     }

  //   },
  //   {
  //     list: [
  //       {

  //       },
  //       {

  //       }
  //     ],
  //     row: {
  //       location: '华寓002'
  //     }
  //   }
  // ]

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getsms()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})