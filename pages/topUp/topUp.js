const app = getApp()

Page({
  data: {
    textArray:[],
    chargeCode:'',
    disabled: true,
  },
  onLoad:function(options){
    var that = this;
    var params = { token: app.data.info.token,};
    app.post("getprice", params, function (data) {
      if (app.data.info.Phone != null || app.data.info.Phone != "0" || app.data.info.Phone != "") {
        console.log(data)
        that.setData({
          textArray:data
        })
      }
    });
  },
  //点击金额
  swicthtap: function (e) {
    let id = e.currentTarget.dataset.id,
      index = parseInt(e.currentTarget.dataset.index),
      num = parseInt(e.currentTarget.dataset.index),    
      chargeCode = e.currentTarget.dataset.item.chargeCode
    var that = this
    this.setData({
      num: index,
      chargeCode: chargeCode,
      disabled: false,      
    })
  },
  //充值支付
  topUp:function(e){
    var that = this;
    var params = {
      token: app.data.info.token,
      chargeCode: that.data.chargeCode,
    };
    app.post("pay", params, function (data) {
      var payModel = data;
      wx.requestPayment({
        'timeStamp': '' + payModel.timeStamp,
        'nonceStr': payModel.nonceStr,
        'package': payModel.package,
        'signType': 'MD5',
        'paySign': payModel.paySign,
        'success': function (res) {
       
          wx.showToast({
            title: '支付成功',
            icon: 'success',
            duration: 2000
          })
          that.setData({
            num: -1,
            disabled: true,
          })
          setTimeout(function () {
            wx.redirectTo({
              url: '/pages/user/user'
            })
          },2000) 
          
        },
        'fail': function (res) {
          console.log(res)
          wx.showToast({
            title: '支付失败',
            duration: 2000,
            icon:'none'
          })
        }
      })
    });
  },

  
})