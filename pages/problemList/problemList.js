// pages/problemList/problemList.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.immediately()
  },

  // 页面渲染即刷新数据
  immediately:function() {
    var that = this;
    var token = app.data.token;
    var params = { token: token, page: '', size: '', keyValue: '', 'type': 1 };
    app.post("batchlist", params, function (data) {
      that.setData({
        list: data
      })
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },
  hrefbtn:function(){
    wx.removeStorageSync('batchId');
    wx.removeStorageSync('dataList');
    wx.navigateTo({
      url: '../search/search'
    })
  },
  oneMore:function(e){
    //return;
    var id = e.currentTarget.dataset.index,
      index = parseInt(e.currentTarget.dataset.index),
      num = parseInt(e.currentTarget.dataset.index),
      name = e.currentTarget.dataset.item.shopName,
      shopid = e.currentTarget.dataset.item.shopId,
      address = e.currentTarget.dataset.item.shopAddress,
      shopphone = e.currentTarget.dataset.item.shopPhone,
      price = e.currentTarget.dataset.item.price,
      batchId = e.currentTarget.dataset.item.batchId

    var item = JSON.stringify(e.currentTarget.dataset.item)
    
    app.data.shopName = name;
    wx.setStorageSync('shopName', name)

    app.data.shopId = shopid;
    wx.setStorageSync('shopId', shopid)

    app.data.shopAddress = address;
    wx.setStorageSync('shopAddress', address)

    app.data.shopPhone = shopphone;
    wx.setStorageSync('shopPhone', shopphone)

    app.data.price = price;
    wx.setStorageSync('price', price)

    app.data.batchId = batchId;
   
    
    var params = {};
  
    params.token = app.data.token;
    params.batchId = batchId;
    params.type = 0; 
    app.post("batchid", params, function (data) {
      wx.setStorageSync('batchId', batchId);
      wx.setStorageSync('dataList', data.dataList);
      wx.navigateTo({
        url: '../delivery/delivery'
      })
    }) 
   
  },
  // 批量删除
  delClick:function(e){
    var that = this;
    var batchId = e.currentTarget.dataset.batchid;
    var params = {};
    params.token = app.data.token;
    params.batchId = batchId;
    params.type = 0;
    wx.showModal({
      title: '操作提醒',
      content: '您正在删除投递信息，确定要删除吗？',
      success: function(res) {
        if (res.confirm) {
          wx.request({
            url: 'https://mapi.shequbow.com/updorders',
            data: params,
            header: { 'Content-Type': 'application/json' },
            method: 'post',
            success: function (res) {
              
              wx.showToast({
                title: '删除成功',
                icon: 'success',
                duration: 1000,
                success: function () {
                  setTimeout(function(){
                    wx.removeStorageSync('batchId');
                    wx.removeStorageSync('dataList');
                    // wx.navigateTo({
                    //   url: '../problemList/problemList'
                    // })
                    that.immediately()
                  },1000)
                }
              })
            }
          }) 
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
    
  }
})