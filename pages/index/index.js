// pages/delivery/delivery.js
const app = getApp()
var expressData = {};
Page({
  data: {
    mapUrl: '../../image/icon-weizhi.png',
    searchUrl: '../../image/search.png',
    awbArray: [],//数组
    expressData: {},//列表
    expressId: '',//运单id
    phone: '',//手机号
    shopName: '请选择门店',//商铺名
    Qr_num: '0',//投递件数
    Qr_dan: '0',//总价格
    showModalStatus: false,//弹窗
    showModalStatus1: false,//扫码输入手机号弹窗
    showModalStatus2: false,//修改手机号或运单号
    resultExpressId: '',//扫码结果
    tabbar: {},//tabbar导航
    modifyEesult:'',//修改扫码单号
    modifyPhone:'',//修改手机号
    focus1:false,
    focus2:false,
    focus3:false,
    overStatus:false,
    overNUM:''
  },
  onLoad: function (options) {
    if (options&&options.scene) {
      console.log('扫码进入了')
      app.data.scene = options.scene;
      wx.setStorageSync('scene', app.data.scene)
      wx.redirectTo({
        url: '../scanStore/scanStore'
      })
    }
  },
  onShow: function () {
    if (!wx.getStorageSync('phoneStorage')) {
      wx.setStorageSync('phoneStorage', [])
    }
    
    var that = this;
    that.setData({
      shopName: app.data.shopName,
      showModalStatus: app.data.showModalStatus,
      showModalStatus1: app.data.showModalStatus1,
      resultExpressId: app.data.resultExpressId
    })
    // app.editTabBar();
    app.checkLogin();
    if (app.data.dataList && app.data.number && app.data.total) {
      that.setData({
        awbArray: app.data.dataList,
        Qr_num: app.data.number,
        Qr_dan: (app.data.number * (app.data.price / 100)).toFixed(2),
      })
      app.data.total = that.data.Qr_dan
      wx.setStorageSync('total', app.data.total)
    };
    
  },
  // 选择门店
  getShop: function () {
    wx.navigateTo({
      url: '../search/search'
    })
  },
  deliveryRecord: function () {
    wx.navigateTo({
      url: '../queryCourier/queryCourier'
    })
  },
  //获取运单号
  expressIdInput: function (e) {
    this.setData({
      expressId: e.detail.value
    })
  },
  // 扫码单号
  resultInput:function(e){
    this.setData({
      resultExpressId: e.detail.value
    })
    app.data.resultExpressId = this.data.resultExpressId
    wx.setStorageSync('resultExpressId', app.data.resultExpressId)
  },
  // 扫码输入手机号
  phoneInput: function (e) {
    this.setData({
      phone: e.detail.value
    })
  },
  // 扫码确认提交
  scanInput: function (e) {
    var that = this;
    var regphone = /^1[0-9]{10}$/;
    if (!regphone.test(that.data.phone)) {
      wx.showToast({
        title: '手机号有误！',
        duration: 2000
      })
      that.setData({
        showModalStatus: true,
        showModalStatus1: true,
        showModalStatus2: false,
      })
    } else if (that.data.phone.length == '') {
      wx.showToast({
        title: '请输入的手机号',
        duration: 2000
      })
      that.setData({
        showModalStatus: true,
        showModalStatus1: true,
        showModalStatus2: false,
      })
    } else if (that.data.phone.length < 11) {
      wx.showToast({
        title: '手机号长度有误！',
        duration: 2000
      })
      that.setData({
        showModalStatus: true,
        showModalStatus1: true,
        showModalStatus2: false,
      })
    } else if (that.data.shopName == '请选择门店' && app.data.shopId == '') {
      wx.showToast({
        title: '请选择门店',
        duration: 2000
      })
      that.setData({
        showModalStatus: true,
        showModalStatus1: true,
        showModalStatus2: false,
      })
    } else {
      that.Qr_num = that.data.awbArray.length + 1;
      that.Qr_dan = that.Qr_num * (app.data.price / 100);
      that.data.awbArray.push({ "expressId": app.data.resultExpressId, "phone": that.data.phone });
      that.setData({
        showModalStatus: false,
        showModalStatus1: false,
        showModalStatus2: false,
        expressId: '',
        phone: '',
        focus: false,
        awbArray: that.data.awbArray,
        Qr_num: that.Qr_num,
        Qr_dan: that.Qr_dan.toFixed(2)
      })
      expressData.dataList = that.data.awbArray

      app.data.dataList = expressData.dataList
      wx.setStorageSync('dataList', expressData.dataList)

      app.data.number = that.Qr_num
      wx.setStorageSync('number', that.Qr_num)

      app.data.total = that.Qr_dan.toFixed(2)
      wx.setStorageSync('total', app.data.total)

      app.data.showModalStatus = false;
      app.data.showModalStatus1 = false;
    }

  },
  //删除
  cancel: function (e) {
    var that = this;
    app.data.number = app.data.dataList.length - 1;
    app.data.total = app.data.total - (app.data.price / 100);
    var index = e.currentTarget.dataset.index;
    app.data.dataList.splice(index, 1)
    app.data.dataList = app.data.dataList;
    wx.setStorageSync('dataList', app.data.dataList)

    app.data.number = app.data.number
    wx.setStorageSync('number', app.data.number)

    app.data.total = (app.data.total).toFixed(2)
    wx.setStorageSync('total', app.data.total)

    that.setData({
      awbArray: app.data.dataList,
      Qr_num: app.data.number,
      Qr_dan: app.data.total,
    })
  },
  //弹窗
  powerDrawer: function (e) {
    this.setData({
      showModalStatus: false,
      showModalStatus1: false,
      showModalStatus2: false,
    });
    app.data.showModalStatus = false;
    app.data.showModalStatus1 = false;
  },
  pushAwb: function () {
    this.setData({
      showModalStatus: false,
      showModalStatus1: false,
      showModalStatus2: false,
    })
    app.data.showModalStatus = false;
    app.data.showModalStatus1 = false;
  },
  formReset: function () {
    this.setData({
      showModalStatus: false,
      showModalStatus1: false,
      showModalStatus2: false,
    })
    app.data.showModalStatus = false;
    app.data.showModalStatus1 = false;
  },
  //扫码
  scan: function (e) {
    var that = this;
    wx.scanCode({
      success: function (res) {
        expressData.dataList = that.data.awbArray
        var result = /^[0-9A-Za-z/-]{8,32}$/;
        app.data.resultExpressId = res.result
        wx.setStorageSync('resultExpressId', app.data.resultExpressId)
        if (app.data.dataList && app.data.dataList !=''){
          var filterarr = app.data.dataList.filter((item) => {
           return item.expressId == app.data.resultExpressId
          })
          if (filterarr.length != 0){
            wx.showToast({
              title: '单号已存在',
              duration: 2000
            })
            that.setData({
              focus1: true,
              showModalStatus: false,
              showModalStatus1: false,
              showModalStatus2: false,
            })  
          }else{
            if (result.test(res.result)) {
              that.getPhone();
              that.setData({
                focus1: false,
                focus: false,
              })
            }
          }
        }else{
          
          if (result.test(res.result)) {
            that.getPhone();
            that.setData({
              focus1: false,
              focus: false,
            })
          }
        }     
      }, fail: function (res) {
        wx.showToast({
          title: '失败',
          duration: 2000
        })
      },
    })
  },
  //手机号转换
  getPhone:function(){
    var that =  this;
      var params = {
        token: app.data.info.token,
        epid: app.data.resultExpressId,
      };
      app.post("expressphone", params, function (data) {
        that.setData({
          showModalStatus1: true,
          showModalStatus: true,
          resultExpressId: app.data.resultExpressId,
        })
        app.data.showModalStatus = true;
        app.data.showModalStatus1 = true;
        if (data.phone == '') {
          that.setData({
            focus: true,
          })
        } else {
          that.setData({
            phone: data.phone,
            focus: false,
          })
        }
      });
  },
  // 点击当前修改
  modifyCurrent:function(e){
    var id = e.currentTarget.dataset.index,
      index = parseInt(e.currentTarget.dataset.index),
      num = parseInt(e.currentTarget.dataset.index),
      item = JSON.stringify(e.currentTarget.dataset.item),
      expressId = e.currentTarget.dataset.item.expressId,
      phone = e.currentTarget.dataset.item.phone
    this.curIndex = parseInt(e.currentTarget.dataset.index)
    var that = this
    this.setData({
      item: item,
      curNavId: id,
      curIndex: index,
      num: index,
      showModalStatus:true,
      showModalStatus1:false,
      showModalStatus2: true,
      modifyEesult: expressId,
      modifyPhone:phone,
      awbArray:app.data.dataList,
      focus2: true,
    })
  },
  // 修改运单号
  modifyEesult:function(e){
    var that = this;
    that.setData({
      modifyEesult: e.detail.value,
      awbArray: that.data.awbArray,
      
    })  
    that.data.awbArray[that.data.curIndex].expressId = that.data.modifyEesult;
    app.data.dataList = that.data.awbArray;
    wx.setStorageSync('dataList', app.data.dataList)
  },
  // 修改手机号
  modifyPhone: function (e){ 
    var that = this;
    that.setData({
      modifyPhone: e.detail.value,
      awbArray: that.data.awbArray,
    })
    that.data.awbArray[that.data.curIndex].phone = that.data.modifyPhone;
    app.data.dataList = that.data.awbArray;
    wx.setStorageSync('dataList', app.data.dataList)
  },
  //确认修改
  Modify:function(){
    var that = this;
    that.setData({
      showModalStatus: false,
      showModalStatus1: false,
      showModalStatus2: false,
      expressId: that.data.modifyEesult,
      phone: that.data.modifyPhone,  
      awbArray: that.data.awbArray,
      focus2:false,
      focus3: false,
    })    
    app.data.showModalStatus = false;
    app.data.showModalStatus1 = false;
  },
  //确认投递
  submitDev: function () {
    var that = this;
    var expressDataStr = JSON.stringify(expressData);
    if (app.data.number == 0) {
      wx.showToast({
        title: '请添加快件投递',
        icon: 'success',
        duration: 2000
      })
    } else {
      wx.request({
        url: "https://mapi.shequbow.com/batch/entry/express",
        data: {
          token: app.data.info.token,
          expressData: expressDataStr,
          shopAddress: app.data.shopAddress,
          shopId: app.data.shopId,
          shopName: app.data.shopName,
          shopPhone: app.data.shopPhone
        },
        method: "POST",
        header: {
          'content-type': 'application/x-www-form-urlencoded'
        },
        success: res => {
          var code = res.data.code, msg = res.data.msg, data = res.data.data;
          if (code == "10000") {
            wx.showToast({
              title: '投递成功',
              icon: 'success',
              duration: 1000
            })
            setTimeout(function () {
              wx.navigateTo({
                url: '../deliveryRecord/deliveryRecord'
              })
              that.setData({
                awbArray: [],
                Qr_num: '0',
                Qr_dan: '0',
              })
            }, 1000)

            app.data.dataList = [];
            wx.setStorageSync('dataList', app.data.dataList)

            app.data.number = '0'
            wx.setStorageSync('number', app.data.number)

            app.data.total = '0'
            wx.setStorageSync('total', app.data.total)
            
          } else if (code == "10003") {
            wx.showToast({
              title: '余额不足',
              duration: 1000
            })
            setTimeout(function () {
              wx.navigateTo({
                url: "../topUp/topUp"
              })
            }, 1000)

          } else {
            wx.showToast({
              title: msg,
            })
          }
        }
      })
      app.data.showModalStatus = false;
      app.data.showModalStatus1 = false;
    }
  },
  //转发
  onShareAppMessage: function (res) {
    if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log(res.target)
    }
    return {
      title: '鸟巢管家',
      path: '/pages/index/index',
      success: function (res) {
        // 转发成功
      },
      fail: function (res) {
        // 转发失败
      }
    }
  },

  deliveryClick:function(){
    let data = [
      {
        img:'/image/newVersion/icon_mendiantoudi@2x.png',
        title:'门店投递',
        text1: '门店投递',
        text2:'',
        bind:'toStore'
      },
      {
        img: '/image/newVersion/icon_toudianjilu@2x.png',
        title: '投店记录',
        text1: '查询投店记录',
        text2: '',
        bind: 'toStoreRecord'
      },
      {
        img: '/image/newVersion/icon_yuqi@2x.png',
        title: '逾期记录',
        text1: '已逾期:',
        text2:'',
        bind: 'toOverdue'
      },
      
    ];
    wx.navigateTo({
      url: '/pages/chestSend/chestSend?module='+ JSON.stringify(data),
    })
    // var params = { token: app.data.token, page: '', size: '', keyValue: '', 'type': 1 };
    // app.post("batchlist", params, function (data) {
    //   if(data.data.length !== 0){
    //     wx.navigateTo({
    //       url: '../problemList/problemList'
    //     })
    //   }
    //   else{
    //     wx.navigateTo({
    //       url: '../search/search'
    //     })
    //   }
    // })
   
  },
  checkClick:function(){
    wx.navigateTo({
      url: '../deliveryRecord/deliveryRecord'
    })
    /*
    wx.navigateTo({
      url: '../queryCourier/queryCourier'
    })
    */
  },
  overdueClick:function(){
    wx.navigateTo({
      url: '../Overdue/Overdue'
    })
  },
  // 派件
  sendGoods: function() {
    wx.navigateTo({
      // url: '/pages/searchWaybill/waybill',
      url:'/pages/chestSend/chestSend'
    })
  },

  // 跳转我的页面
  toUser: function(){
    wx.redirectTo({
      url: '/pages/user/user',
    })
  },
})