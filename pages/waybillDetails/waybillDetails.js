// pages/waybillDetails/waybillDetails.js
const app = getApp()
var time = require("../../utils/util.js");

Page({
  data: {
    expressId: '',//运单id
    expressNum: '',//运单号
    phone:'',//收件人电话
    frameKey:'',//货架号
    keyCode: '',//取件码
    shopPhone:'',//门店电话
    expressPosition:'',//快件位置
    signType:'',//投递类型
    expressState:'',//快件状态
    createDate: '',//投递时间
    dateSigned: '',//上架时间
    dateCollect: '',//入库时间
    dateSend: '',//出库时间
    delieverType:'',//投递类型
    showModalStatus:false,//弹窗
    showModalStatus1: false,//弹窗
    showModalStatus2: false,//弹窗
    showModalStatus3: false,//弹窗
    expressArr:{},
    countDownnum: 300//发短信倒数计时5分钟
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    var expressData = JSON.parse(options.item);
    that.data.expressArr = expressData;
    // console.log(expressData)
    if (app.data.info.Phone) {
      var params = {
        token: app.data.info.token,
        expressId: that.data.expressArr.expressId
      };
      app.post("batchdetail", params, function (data) {
        var list = data;
        console.log(list)
        if (list.msgStatus == '0') {
          list.msgStatusCN = '发送中'
        }
        else if (list.msgStatus == '1') {
          list.msgStatusCN = '发送成功'
        }
        else if (list.msgStatus == 2) {
          list.msgStatusCN = '发送失败'
        }
        if (list.msgType == 0)
        {
          list.msgTypeCn ='短信'
        }
        else if(list.msgType == 1){
          list.msgTypeCn ='微信'
        }

        if (list.operateType == '0'){
          list.operateType = '老板录件'
        }
        else if (list.operateType == '1'){
          list.operateType = '快递员录件'
        }

        that.setData({
          expressNum: data.expressNum,
          shopPhone: data.shopPhone,
          expressPosition: data.expressPosition,
          frameKey: data.frameKey,
          createDate: data.createDate,          
          datePuton: data.datePuton,
          dateCollect: data.dateCollect,
          datePulloff: data.datePulloff,
          msgTypeCn:list.msgTypeCn,
          operateType:list.operateType,
          keyCode: list.keyCode,
          delieverType: list.delieverType,
          msgStatusCN:list.msgStatusCN
        })
        if (data.expressState == 0) {
          that.setData({
            expressState: '入库交接'
          })
        } else if (data.expressState == 1) {
          that.setData({
            expressState: '签收'
          })
        } else if (data.expressState == 2) {
          that.setData({
            expressState: '退件'
          })
        } else if (data.expressState == 3) {
          that.setData({
            expressState: '待处理'
          })
        } else if (data.expressState == 4) {
          that.setData({
            expressState: '签收,'
          })
        } else if (data.expressState == 6) {
          that.setData({
            expressState: '快递员撤回'
          })
        } else if (data.expressState == 7) {
          that.setData({
            expressState: '销件'
          })
        } else if (data.expressState == 9) {
          that.setData({
            expressState: '合单确认'
          })
        } else {
          that.setData({
            expressState: '其他'
          })
        }
        //签收类型
        if (data.signType == 0) {
          that.setData({
            signType: '正常'
          })
        } else if (data.signType == 1) {
          that.setData({
            signType: '投店'
          })
        } else if (data.signType == 2) {
          that.setData({
            signType: '投柜'
          })
        } else if (data.signType == 3) {
          that.setData({
            signType: '破损件'
          })
        } else if (data.signType == 4) {
          that.setData({
            signType: '错分件,'
          })
        } else if (data.signType == 5) {
          that.setData({
            signType: '送无人电话不接'
          })
        } else if (data.signType == 6) {
          that.setData({
            signType: '拒收'
          })
        } else if (data.signType == 7) {
          that.setData({
            signType: '预约派送'
          })
        } else if (data.signType == 8) {
          that.setData({
            signType: '改地址'
          })
        } else if (data.signType == 9) {
          that.setData({
            signType: '其他异常'
          })
        } else {
          that.setData({
            signType: '未知异常'
          })
        }
        //门店电话
        if (data.shopPhone == null || data.shopPhone == '') {
          that.setData({
            shopPhone: '--'
          })
        } else {
          that.setData({
            shopPhone: data.shopPhone
          })
        }
        //手机号
        if (data.phone == 0) {
          that.setData({
            phone: '--'
          })
        } else {
          that.setData({
            phone: data.phone
          })
        }
      });
      }
  },
  // 修改收件人电话
  Modifyphone:function(){
    var that = this;
    that.setData({
      showModalStatus:true,
      showModalStatus1: true,
      modifyRp:that.data.phone,
    })
  },
  //修改运单号
  Modifynum: function () {
    var that = this;
    that.setData({
      showModalStatus: true,
      showModalStatus2: true,
      modifyAwb: that.data.expressNum,
    })
  },
  //修改货架号
  ModifyFramekey: function () {
    var that = this;
    that.setData({
      showModalStatus: true,
      showModalStatus3: true,
      modifyFrameKey: that.data.frameKey,
    })
  },
  // 获取收件人电话
  recipient:function(e){
    this.setData({
      modifyRp: e.detail.value
    })
  },
  // 获取运单号
  waybill: function (e) {
    this.setData({
      modifyAwb: e.detail.value
    })
  },
  // 获取货架号
  frameKey: function (e) {
    this.setData({
      modifyFrameKey: e.detail.value
    })
    console.log(e.detail.value)
  },
  // 确认修改收件人电话
  Modify: function () {
    var that = this;
    var params = { 
      token: app.data.info.token,
      expressId: that.data.expressArr.expressId,
      phone: that.data.modifyRp,
    };
    app.post("updphone", params, function (data) {    
        that.setData({
          showModalStatus: false,
          showModalStatus1: false,
          phone: that.data.modifyRp,
        })  
    });
  },
  // 确认修改运单号
  ModifyAwb:function(){
    var that = this;
    var params = {
      token: app.data.info.token,
      expressId: that.data.expressArr.expressId,
      expressNum: that.data.modifyAwb,
    };
    app.post("updexpressnum", params, function (data) {
      that.setData({
        showModalStatus: false,
        showModalStatus2: false,
        expressNum: that.data.modifyAwb,
      })
    });
  },
  // 确认修改货架号
  ModifyFrameKey: function () {
    var that = this;
    var params = {
      token: app.data.info.token,
      expressId: that.data.expressArr.expressId,
      frameKey: that.data.modifyFrameKey,
    };
    app.post("updframekey", params, function (data) {
      that.setData({
        showModalStatus: false,
        showModalStatus3: false,
        frameKey: that.data.modifyFrameKey,
      })
    });
   },
  //弹窗
  powerDrawer: function (e) {
    this.setData({
      showModalStatus: false,
      showModalStatus1: false,
      showModalStatus2: false,
      showModalStatus3: false,
    });
  },
  formReset: function () {
    this.setData({
      showModalStatus: false,
      showModalStatus1: false,
      showModalStatus2: false,
      showModalStatus3: false,
    })
  },
  resendMsg:function(){
    console.log('进入了重发短信')
    wx.showLoading({
      title: '请稍后',
    })
    var data = this.data,
      expressArr = data.expressArr || { expressId:0},
      expressId = expressArr.expressId;
    console.log(expressId)
    app.post("resendmsg", { token: app.data.info.token, expressId: expressId},function(data){
      wx.hideLoading()
      wx.showToast({
        title: '短信发送成功',
      })
    });

    var that = this;
    var countDown = setInterval(function () {
      that.data.countDownnum -= 1;
      that.setData({
        countDownnum: that.data.countDownnum
      })
      if (that.data.countDownnum == 0) {
        clearInterval(countDown)
      }
      // console.log(that.data.countDownnum)
    }, 1000)
  },

  /**
  * 生命周期函数--监听页面卸载
  */
  onUnload: function () {
    clearInterval(this.resendMsg.countDown)
  },

})